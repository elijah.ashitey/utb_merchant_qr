import 'package:flutter/material.dart';
import 'package:qr_approval/models/historyModel.dart';
import 'package:qr_approval/models/pendingModel.dart' as pending;
// import 'package:qr_approval/models/trans.dart';
import 'package:qr_approval/screens/approveScreen.dart';
import 'package:qr_approval/screens/changePin.dart';
import 'package:qr_approval/screens/changepassword.dart';
import 'package:qr_approval/screens/firstTImeLogin.dart';

import 'package:qr_approval/screens/homePage.dart';
import 'package:qr_approval/screens/lockScreen.dart';
import 'package:qr_approval/screens/loginPage.dart';
import 'package:qr_approval/screens/pinConfigPage.dart';
import 'package:qr_approval/screens/rejectreason.dart';
import 'package:qr_approval/screens/splashScreen.dart';
import 'package:qr_approval/screens/sucPage.dart';
import 'package:qr_approval/screens/transHistoryPage.dart';
import 'package:qr_approval/screens/viewQR.dart';

import 'routeConstants.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case SplashScreenRoute:
      return MaterialPageRoute(builder: (context) => SplashScreen());
      break;
    case LoginPageRoute:
      return MaterialPageRoute(builder: (context) => LoginPage());
      break;
    case HomePageRoute:
      return MaterialPageRoute(builder: (context) => HomePage());
      break;
    case LockScreenRoute:
      return MaterialPageRoute(builder: (context) => LockScreenPage());
      break;
    case ApproveRoute:
      final pending.Datum arg = settings.arguments;

      return MaterialPageRoute(builder: (context) => ApproveScreen(arg));
      break;
    case TransHistoryRoute:
      final Datum arg = settings.arguments;

      return MaterialPageRoute(builder: (context) => TransHistoryScreen(arg));
      break;
    case SuccessRoute:
      final arg = settings.arguments;

      return MaterialPageRoute(builder: (context) => SuccessPage(arg));
      break;
    case RejectCommentRoute:
      return MaterialPageRoute(builder: (context) => RejectCommentPage());
      break;
    case PinPageRoute:
      return MaterialPageRoute(builder: (context) => PinConfigScreen());
      break;
    case ChangePasswordRoute:
      return MaterialPageRoute(builder: (context) => ChangePassword());
      break;
    case ChangePinRoute:
      return MaterialPageRoute(builder: (context) => ChangePin());
      break;
    case ViewQRRoute:
      return MaterialPageRoute(builder: (context) => VIewQR());
      break;
    case FirstTimeRoute:
      final arg = settings.arguments;
      return MaterialPageRoute(builder: (context) => FirstTimeLogin(arg));
      break;
    default:
      return MaterialPageRoute(builder: (context) => LoginPage());
  }
}
