import 'package:flutter/material.dart';
import 'package:qr_approval/models/pendingModel.dart';
// import 'dart:async';

class Data with ChangeNotifier {
  Datum _data;
  String _pending = " ";
  bool _moreData = false;
  bool get getMoreDate => _moreData;
  String get getPending => _pending;
  Datum get getData => _data;
  setData(Datum load) {
    _data = load;
    notifyListeners();
  }

  setPending(List<Datum> load) {
    // Timer _timer;
    // int _start = 10;
    int pending = load.length;
    _pending = pending.toString();
    if (pending > 1) {
      _moreData = true;
    } else {
      _moreData = false;
    }
    /* if (pending > 1) {
      print("Initial setting:" + _moreData.toString());

      _moreData = true;
      if (_moreData) {
        Future.delayed(const Duration(minutes: 1), () {
          print("delay is over, i want to set to false");
          _moreData = false;
          print("setting to false:" + _moreData.toString());
        });
      }
      /*   if (_timer != null) {
        _timer.cancel();
      }

      _timer = Timer(
          const Duration(seconds: 2),
          () => () {
                print("time is up");
                _moreData = false;
              });*/
*/
    // const oneSec = const Duration(seconds: 1);
    // _timer = Timer.periodic(
    //   oneSec,
    //   (Timer timer) => () {
    //     if (_start < 1) {
    //       timer.cancel();
    //     } else {
    //       _start = _start - 1;
    //     }
    //   },
    // );
  }
  // notifyListeners();
  // }
}
