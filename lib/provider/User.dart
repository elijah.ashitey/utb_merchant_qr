import 'package:flutter/material.dart';
import 'package:qr_approval/models/loginModel.dart';
import 'package:qr_approval/models/pendingModel.dart';
import 'package:qr_approval/resources/apiCalls.dart';

class UserInfo with ChangeNotifier {
  LoginModel _data;
  LoginModel get getUser => _data;
  setUser(LoginModel data) {
    _data = data;
    notifyListeners();
  }

  PendingModel _pending;
  PendingModel get getData => _pending;
  setPending(PendingModel data) {
    _pending = data;
    notifyListeners();
  }

  Future<PendingModel> refresh() async {
    // String name = getuser.globalUsername;

    String id = _data.data.userId;
    await Future.delayed(Duration(seconds: 2));
    var result = await pending(id);
    // int pastPending = getPending;
    if (result != null) {
      setPending(result);
      notifyListeners();
      return result;
    } else {
      return null;
    }
  }
}
