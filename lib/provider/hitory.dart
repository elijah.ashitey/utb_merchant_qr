import 'package:flutter/material.dart';
import 'package:qr_approval/models/dailyHistory.dart';
import 'package:qr_approval/models/historyModel.dart';

class HistoryData with ChangeNotifier {
  String _rejected;
  String _approved;
  Datum _data;
  Data _dateDate;
  Datum get getData => _data;
  Data get getDataByDate => _dateDate;
  String get getRjected => _rejected;
  String get getGetApproved => _approved;
  setData(Datum load) {
    _data = load;
    notifyListeners();
  }

  setDataByDate(Data load) {
    _dateDate = load;
    notifyListeners();
  }

  setRejected(List<Datum> data) {
    print("provider Data Recieved");
    int reject = 0;
    int approve = 0;
    // int pending = 0;
    for (var i = 0; i <= data.length - 1; i++) {
      if (data[i].statusFlag == "N") {
        reject = reject + 1;
      } else if (data[i].statusFlag == "Y" ||
          data[i].statusFlag == "p" ||
          data[i].statusFlag == "P") {
        approve = approve + 1;
      }
    }

    _rejected = reject.toString();
    _approved = approve.toString();
    print(_rejected);
    print(_approved);
    // notifyListeners();
  }
}
