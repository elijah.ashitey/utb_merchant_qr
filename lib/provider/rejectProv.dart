import 'package:flutter/material.dart';

class RejectReasonList with ChangeNotifier {
  List<RejectReason> _details;
  RejectReason _comment;

  List<RejectReason> get getReasons => _details;
  RejectReason get getComment => _comment;

  setReasons(List<RejectReason> res) {
    _details = res;
    notifyListeners();
  }

  setComment(RejectReason res) {
    _comment = res;
    notifyListeners();
  }
}

class RejectReasonProvider with ChangeNotifier {
  String _comment = " ";
  int _code;

  String get getComment => _comment;
  int get getCode => _code;

  setComment(String res) {
    _comment = res;
    notifyListeners();
  }

  setCode(int res) {
    _code = res;
    notifyListeners();
  }
}

class RejectReason {
  final String reject;
  final int code;
  RejectReason(this.reject, this.code);
}

final RejectReason one = RejectReason("Wrong amount. Less than expected", 1);
final RejectReason two = RejectReason("Wrong amount. More than expected", 2);
final RejectReason three = RejectReason("In-active user", 3);
// final RejectReason four = RejectReason("reason 4", 4);
final RejectReason last = RejectReason("Other", 00);
List<RejectReason> rejectreasonData = [one, two, three, last];
