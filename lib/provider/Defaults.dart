import 'package:flutter/material.dart';

class Default with ChangeNotifier {
  bool _isloading = false;
  bool _timeOut = false;
  bool _closureoff = false;
  // int _closure = 0;
  String _pin = "";
  String _currency = "SLL";
  BuildContext _context;
  BuildContext get getContext => _context;
  bool get getIsLoadind => _isloading;
  bool get getTimeOut => _timeOut;
  bool get getClosureOff => _closureoff;
  String get getPin => _pin;
  String get getCurrency => _currency;
  // int get getClosure => _closure;
  setLoading(bool load) {
    _isloading = load;
    notifyListeners();
  }

  setClosureOff(bool load) {
    _closureoff = load;
    notifyListeners();
  }

  setPin(String load) {
    _pin = load;
    notifyListeners();
  }

  setCurrency(String load) {
    _currency = load;
    notifyListeners();
  }

  // setClosure(int load) {
  //   if (load > _closure) {
  //     _closureoff = true;
  //   }
  //   _closure = load;
  //   notifyListeners();
  // }

  setTimeOut(bool load) {
    _timeOut = load;
    print("time out set at provider" + _timeOut.toString());
    notifyListeners();
  }

  setContext(BuildContext context) {
    _context = context;
    notifyListeners();
  }
}
