import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
// import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/provider/pending.dart';
// import 'package:qr_approval/screens/changepassword.dart';
// import 'package:qr_approval/widgets/lockScreen.dart';

import 'Routes/routes.dart';
import 'provider/Defaults.dart';
import 'provider/User.dart';
import 'provider/hitory.dart';
import 'provider/rejectProv.dart';
import 'resources/connectionService.dart';
import 'screens/ella.dart';
import 'screens/ella2.dart';
import 'screens/firstTImeLogin.dart';
import 'screens/pinConfigPage.dart';
import 'widgets/historyLoading.dart';
// import 'screens/lockScreen.dart';
// import 'screens/pinConfigPage.dart';
// import 'screens/pinPage.dart';
// import 'screens/sucPage.dart';
// import 'widgets/load.dart';
// import 'widgets/lockScreen.dart';
// import 'widgets/pinPage.dart';
// import 'widgets/search.dart';
// import 'widgets/shimmerLoader.dart';
// // import 'screens/lockScreen.dart';
// import 'screens/loginPage.dart';
// import 'screens/pinPage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MultiProvider(providers: [
    ChangeNotifierProvider<Default>.value(value: Default()),
    ChangeNotifierProvider<UserInfo>.value(value: UserInfo()),
    ChangeNotifierProvider<RejectReasonList>.value(value: RejectReasonList()),
    ChangeNotifierProvider<RejectReasonProvider>.value(
        value: RejectReasonProvider()),
    ChangeNotifierProvider<Data>.value(value: Data()),
    ChangeNotifierProvider<HistoryData>.value(value: HistoryData()),
    //   ChangeNotifierProvider<StateRequest>.value(value: StateRequest()),
    //   ChangeNotifierProvider<Cheque>.value(value: Cheque()),
    //   ChangeNotifierProvider<StopChequeProv>.value(value: StopChequeProv()),
    //   ChangeNotifierProvider<BeneProv>.value(value: BeneProv()),
  ], child: MyApp()));
}

class MyApp extends StatefulWidget {
  // static final navKey = GlobalKey<NavigatorState>();
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  Timer _timer;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      final context = _navigatorKey.currentState.overlay.context;
      _initializeTimer(context);
    });
    // _initializeTimer(context);
  }

  // _showDialog() async {
  //   await Future.delayed(Duration(milliseconds: 50));
  //   showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return new Container(child: new Text('foo'));
  //       });
  // }

  void _initializeTimer(BuildContext context) {
    if (_timer != null) {
      _timer.cancel();
    }

    _timer = Timer(const Duration(hours: 36), () => _logOutUser(context));
  }

  void _logOutUser(BuildContext context) {
    _timer?.cancel();
    _timer = null;
    // var timeout = Provider.of<Default>(context, listen: false);
    // timeout.setTimeOut(true);
    print("time out");
    // String username = await loadDataN();
    // Popping all routes and pushing login screen
    _navigatorKey.currentState
        .pushNamed(LockScreenRoute)
        .then((value) => _initializeTimer(context));
  }

  // void _showPushDialog() {
  //   print("DIALOG");
  //   showDialog<bool>(
  //     context: context,
  //     builder: (_) => _buildDialog(context),
  //   ).then((bool shouldNavigate) {
  //     if (shouldNavigate == true) {
  //       _navigateToPushDetail();
  //     }
  //   });
  // }

  void _handleUserInteraction(BuildContext context) {
    _initializeTimer(context);
    // var timeout = Provider.of<Default>(context, listen: false);
    // timeout.setTimeOut(false);
    print("touching");
  }

  @override
  Widget build(BuildContext context) {
    return StreamProvider<ConnectivityStatus>(
      create: (context) =>
          ConnectivityService().connectionStatusController.stream,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => _handleUserInteraction(context),
        onPanDown: (_) => _handleUserInteraction(context),
        child: MaterialApp(
          builder: BotToastInit(),
          navigatorObservers: [BotToastNavigatorObserver()],
          navigatorKey: _navigatorKey,
          debugShowCheckedModeBanner: false,
          title: 'QR_Merchant',
          initialRoute: SplashScreenRoute,
          onGenerateRoute: generateRoute,
          theme: ThemeData(
            textTheme: TextTheme(
              bodyText1: TextStyle(
                fontSize: 17,
              ),
              bodyText2: TextStyle(
                fontSize: 16,
                // fontWeight: FontWeight.bold,
              ),
            ),
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            fontFamily: 'baloo',
          ),
          // home: PinConfigScreen(),
        ),
      ),
    );
  }
}
