import 'package:flutter/material.dart';
import 'package:flutter_screen_lock/lock_screen.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/resources/connectionService.dart';
// import 'package:qr_approval/widgets/lockScreen.dart';
// import 'package:seed/functions/connectionStatus.dart';

class NetworkSensitive extends StatelessWidget {
  final Widget child;
  final double opacity;

  NetworkSensitive({
    this.child,
    this.opacity = 0.5,
  });

  @override
  Widget build(BuildContext context) {
    // Get our connection status from the provider
    var connectionStatus = Provider.of<ConnectivityStatus>(context);
    var timeout = Provider.of<Default>(context);
    if (timeout.getTimeOut) {
      return
          //  RaisedButton(
          //   child: Text('Open biometric first'),
          // onPressed: () =>
          LockScreen(
        // context: context,
        // canCancel: false,
        correctString: '1234',
        canBiometric: true,
        showBiometricFirst: true,
        biometricFunction: (context) async {
          // var didAuthenticate = await authenticate();
          var localAuth = LocalAuthentication();
          var didAuthenticate = await localAuth.authenticateWithBiometrics(
              localizedReason: 'Please authenticate to show account balance');
          if (didAuthenticate) {
            timeout.setTimeOut(false);
            await Navigator.of(context).maybePop();
          }
        },
      );
      // );
    }
    if (connectionStatus == ConnectivityStatus.WiFi ||
        connectionStatus == ConnectivityStatus.Cellular) {
      return child;
    }
    return Scaffold(
      body: Center(
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Icon(
                  Icons.cloud_off,
                  size: 50,
                  color: Colors.grey,
                ),
              ),
              Text(
                "You are Offline",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Please Check your internet connection",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
    /* if (connectionStatus == ConnectivityStatus.Cellular) {
      return Opacity(
        opacity: opacity,
        child: child,
      );
    }
    
*/
    // return Opacity(
    //   opacity: 0.1,
    //   child: child,
    // );
  }
}
