import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

// info(BuildContext context, String message) {
//   AwesomeDialog(
//           context: context,
//           dialogType: DialogType.INFO,
//           animType: AnimType.TOPSLIDE,
//           tittle: '',
//           body: Padding(
//             padding: const EdgeInsets.all(8),
//             child: Text(
//               message,
//               textAlign: TextAlign.center,
//               style: Theme.of(context).textTheme.headline3,
//             ),
//           ),
//           // desc: message,
//           // btnCancelOnPress: () {},
//           btnOkOnPress: () {})
//       .show();
// }

/*
confirm(BuildContext context, String message, int code) {
  final _benInfo = Provider.of<BeneProv>(context, listen: false);
  final _userInfo = Provider.of<UserInfo>(context);

  AwesomeDialog(
      context: context,
      dialogType: DialogType.WARNING,
      animType: AnimType.TOPSLIDE,
      tittle: '',
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.black, fontSize: 18),
        ),
      ),
      // desc: message,
      btnCancelOnPress: () {},
      btnOkOnPress: () async {
        if (code == 1) {
          var result = await Connectivity().checkConnectivity();

          if (result == ConnectivityResult.none) {
            info(context, "please check your internet");
          } else {
            try {
              final resp = await deleteBeneAPI(
                  _benInfo.getBeneInfo.code, _userInfo.getInfo.tokenId);
              if (resp.mbResponse == "00") {
                return success(context, resp.mbResponse, () {});
              } else {
                return error(context, resp.mbResponse);
              }
            } catch (e) {
              return error(context, "Internal Server Error");
            }
          }
        } else {}
      }).show();
}
*/
success(BuildContext context, String message, Function function) {
  AwesomeDialog(
          dismissOnTouchOutside: false,
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.TOPSLIDE,
          tittle: '',
          body: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              message,
              textAlign: TextAlign.center,
              // style: Theme.of(context).textTheme.headline4,
            ),
          ),
          // desc: message,
          // btnCancelOnPress: () {},
          btnOkOnPress: function)
      .show();
}

info(BuildContext context, String message) {
  AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.TOPSLIDE,
          tittle: '',
          body: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black, fontSize: 18),
            ),
          ),
          // desc: message,
          // btnCancelOnPress: () {},
          btnOkOnPress: () {})
      .show();
}

newsuccess(
  BuildContext context,
  String message,
) {
  AwesomeDialog(
          dismissOnTouchOutside: false,
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.TOPSLIDE,
          tittle: '',
          body: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black, fontSize: 18),
            ),
          ),
          // desc: message,
          // btnCancelOnPress: () {},
          btnOkOnPress: () {})
      .show();
}

conf(BuildContext context, String message, Function function) {
  AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.TOPSLIDE,
          tittle: '',
          body: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black, fontSize: 18),
            ),
          ),
          // desc: message,
          btnCancelOnPress: () {},
          btnOkOnPress: function)
      .show();
}

err(BuildContext context, String message) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.ERROR,
    animType: AnimType.SCALE,
    tittle: '',
    body: Padding(
      padding: const EdgeInsets.all(8),
      child: Text(
        message,
        textAlign: TextAlign.center,
        // style: Theme.of(context).textTheme.headline4,
        // TextStyle(color: Colors.black, fontSize: 20, fontFamily: 'gotham'),
      ),
    ),
    // desc: message,
    btnCancelOnPress: () {},
    // btnOkOnPress: () {}
  ).show();
}
