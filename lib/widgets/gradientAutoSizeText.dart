import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class GradientAutoSizeText extends StatelessWidget {
  GradientAutoSizeText(this.data,
      {@required this.gradient, this.style, this.textAlign = TextAlign.left});

  final String data;
  final Gradient gradient;
  final TextStyle style;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (bounds) {
        return gradient.createShader(Offset.zero & bounds.size);
      },
      child: Expanded(
        child: AutoSizeText(
          data,
          textAlign: textAlign,
          maxLines: 2,
          style: (style == null)
              ? TextStyle(color: Colors.white)
              : style.copyWith(color: Colors.white),
        ),
      ),
    );
  }
}
