import 'package:flutter/cupertino.dart';
import 'package:flutter_screen_lock/lock_screen.dart';
import 'package:local_auth/local_auth.dart';
// import 'package:provider/provider.dart';
// import 'package:qr_approval/provider/Defaults.dart';

lockScreen(BuildContext context) {
  // final timeout = Provider.of<Default>(context);

  return showLockScreen(
    context: context,
    // canCancel: false,
    correctString: '1234',
    canBiometric: true,
    showBiometricFirst: true,
    biometricFunction: (context) async {
      // var didAuthenticate = await authenticate();
      var localAuth = LocalAuthentication();
      var didAuthenticate = await localAuth.authenticateWithBiometrics(
          localizedReason: 'Please authenticate to show account balance');
      if (didAuthenticate) {
        // timeout.setTimeOut(false);
        // await Navigator.of(context).maybePop();
      }
    },
  );
}
