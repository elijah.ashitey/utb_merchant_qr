import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:loading_animations/loading_animations.dart';

import 'gradient.dart';

loading({int size}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        LoadingBouncingGrid.square(
          size: size ?? 30,
          backgroundColor: Colors.blue,
        ),
        GradientText("Loading",
            gradient: basic(),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
            textAlign: TextAlign.center),
      ],
    ),
  );
}
