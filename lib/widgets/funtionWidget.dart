String initials(String name) {
  // name.replaceAll(RegExp(r"\s+\b|\b\s"), "~");
  name.replaceAll(" ", "");
  return name.substring(0, 1);
  // List<String> arr = name.split('~');
  // if (arr.length > 1) {
  //   String initials = arr[0].substring(0, 1) + "." + arr[1].substring(0, 1);
  //   return initials;
  // } else if (arr.length == 1) {
  //   String initials = arr[0].substring(0, 1);
  //   //  + "." + arr[1].substring(0, 1);
  //   return initials;
  // } else {
  //   return " . ";
  // }

  // return "A.E";
}

String accNum(String acc) {
  int length = acc.length;
  String start = acc.substring(0, 3);
  String end = acc.substring(length - 3, length);
  int mask = length - 6;
  String hash = "*" * mask;
  return start + hash + end;
}
