import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
// import 'package:qr_approval/screens/approveScreen.dart';
import 'package:qr_approval/screens/pending.dart';

import 'funtionWidget.dart';

Widget pendingShimmer2(
    context, date, time, String name, transaID, String amount,
    {String flag}) {
  Color _color;
  if (flag == "Y" || flag == "P") {
    _color = Colors.green;
  } else if (flag == "N") {
    _color = Colors.redAccent;
  } else {
    _color = Colors.lightBlueAccent;
  }
  return Center(
      child: Padding(
    padding: const EdgeInsets.all(0.0),
    child: ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(6)),
      child: Container(
        padding: EdgeInsets.only(left: 8.0),
        height: MediaQuery.of(context).size.height * 0.16,
        decoration: BoxDecoration(
          // color: Colors.lightBlueAccent,
          color: _color,
          borderRadius: BorderRadius.all(Radius.circular(6)),
        ),
        child: Container(
          color: Colors.white,
          child: Row(
            children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                    // color: Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: CircleAvatar(
                            radius: 19,
                            backgroundColor: _color,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.black,
                              radius: 18,
                              child: Text(initials(name)),
                            ),
                          ),
                        ),
                        Spacer(),
                        Text(
                          date,
                          style: TextStyle(color: Colors.black
                              // fontWeight: FontWeight.bold,
                              ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Text(
                          time,
                          style: TextStyle(
                            color: Colors.black,
                            // fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                      ],
                    ),
                  )),
              VerticalDivider(
                // color: Colors.lightBlueAccent,
                color: _color,
              ),
              Expanded(
                flex: 6,
                child: Container(
                  // color: Colors.cyan,
                  child: Stack(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Spacer(),

                          Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: Text(
                                "Name: " + name,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.black,
                                  // fontWeight: FontWeight.bold,
                                ),
                              )),
                          Spacer(),

                          Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: Text(
                                "Trans ID: " + transaID,
                                style: TextStyle(
                                  color: Colors.black,
                                  // fontWeight: FontWeight.bold,
                                ),
                              )),
                          Spacer(),
                          Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: Text(
                                "Amount: " + amount,
                                style: TextStyle(
                                  color: Colors.black,
                                  // fontWeight: FontWeight.bold,
                                ),
                              )),
                          Spacer(),

                          // Spacer(),
                          // Align(
                          //     alignment: Alignment.bottomRight,
                          //     child: Container(
                          //       height:
                          //           MediaQuery.of(context).size.width * 0.11,
                          //       // width: MediaQuery.of(context).size.width * 0.3,
                          //       decoration: BoxDecoration(
                          //         // color: Colors.blue[300],
                          //         color: _color,

                          //         borderRadius: BorderRadius.only(
                          //             topLeft: Radius.circular(15)),
                          //       ),
                          //       child: Padding(
                          //         padding: const EdgeInsets.all(8.0),
                          //         child: Text(
                          //           amount,
                          //           style: TextStyle(
                          //             fontSize: 18,
                          //             color: Colors.white,
                          //             // fontWeight: FontWeight.bold,
                          //           ),
                          //         ),
                          //       ),
                          //     )),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ),
  ));
}

Widget pendingShimmer(context, date, time, String name, transaID, String amount,
    {String flag}) {
  Color _color;
  if (flag == "Y") {
    _color = Colors.green;
  } else if (flag == "N") {
    _color = Colors.redAccent;
  } else {
    _color = Colors.lightBlueAccent;
  }
  return Center(
      child: Padding(
    padding: const EdgeInsets.all(0.0),
    child: ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(6)),
      child: Container(
        padding: EdgeInsets.only(left: 8.0),
        height: MediaQuery.of(context).size.height * 0.16,
        decoration: BoxDecoration(
          // color: Colors.lightBlueAccent,
          color: _color,
          borderRadius: BorderRadius.all(Radius.circular(6)),
        ),
        child: Container(
          color: Colors.white,
          child: Row(
            children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Container(
                    // color: Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: CircleAvatar(
                            radius: 19,
                            backgroundColor: _color,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.black,
                              radius: 18,
                              child: Text(initials(name)),
                            ),
                          ),
                        ),
                        Spacer(),
                        Text(
                          date,
                          style: TextStyle(color: Colors.black
                              // fontWeight: FontWeight.bold,
                              ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Text(
                          time,
                          style: TextStyle(
                            color: Colors.black,
                            // fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                      ],
                    ),
                  )),
              VerticalDivider(
                // color: Colors.lightBlueAccent,
                color: _color,
              ),
              Expanded(
                flex: 6,
                child: Container(
                  // color: Colors.cyan,
                  child: Stack(
                    children: <Widget>[
                      // Align(
                      //     alignment: Alignment.bottomRight,
                      //     child: Container(
                      //       height: MediaQuery.of(context).size.width * 0.1,
                      //       width: MediaQuery.of(context).size.width * 0.3,
                      //       decoration: BoxDecoration(
                      //         color: Colors.lightBlueAccent,
                      //         borderRadius: BorderRadius.only(
                      //             topLeft: Radius.circular(15)),
                      //       ),
                      //       child: Center(child: Text(amount)),
                      //     )),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 5,
                          ),
                          Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: Text(
                                "Name: " + name,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.black,
                                  // fontWeight: FontWeight.bold,
                                ),
                              )),
                          Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: Text(
                                "Trans ID: " + transaID,
                                style: TextStyle(
                                  color: Colors.black,
                                  // fontWeight: FontWeight.bold,
                                ),
                              )),
                          Spacer(),
                          Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.11,
                                // width: MediaQuery.of(context).size.width * 0.3,
                                decoration: BoxDecoration(
                                  // color: Colors.blue[300],
                                  color: _color,

                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15)),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    amount,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      // fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              )),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ),
  ));
}

Widget historyWidget(context, amt, acc, transid, approver, date, flag) {
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
    child: Card(
      elevation: 8,
      color: Colors.green,
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0, bottom: 2),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0, top: 5),
                child: Column(
                  children: <Widget>[
                    data("Amount:", amt),
                    data("Paid Into:", acc),
                    data("Trans_no:", transid),
                    data("Approved by:", approver),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Container(
                          child: dateTime(Icons.date_range,
                              DateFormat("dd-MMM-yyyy").format(date), context),
                        )),
                        Expanded(
                            child: Container(
                          child:
                              dateTime(Icons.access_time, time(date), context),
                        )),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            flag == "P"
                ? Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: CircleAvatar(
                        backgroundColor: Colors.lightBlueAccent,
                        radius: 15,
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    ),
  );
}

Row data(title, data) {
  return Row(
    children: <Widget>[
      Expanded(
        child: Text(
          title,
          style: TextStyle(color: Colors.black54),
        ),
      ),
      Expanded(
        flex: 2,
        child: Text(
          data,
          style: TextStyle(color: Colors.black),
        ),
      ),
    ],
  );
}

dateTime(icon, text, context) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      children: <Widget>[
        Icon(
          icon,
          size: 20,
          color: Colors.lightBlueAccent,
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          text,
          style: TextStyle(color: Colors.grey),
          // style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    ),
  );
}
