import 'package:flutter/cupertino.dart';

LinearGradient basic() {
  return LinearGradient(
      colors: [
        const Color(0xFF3366FF),
        const Color(0xFF00CCFF),
      ],
      begin: const FractionalOffset(0.0, 0.0),
      end: const FractionalOffset(1.0, 0.0),
      stops: [0.0, 1.0],
      tileMode: TileMode.clamp);
}

LinearGradient top() {
  return LinearGradient(
    colors: [
      const Color(0xFF00CCFF),
      const Color(0xFF3366FF),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    // stops: [0.0, 1.0],
    // tileMode: TileMode.clamp,
  );
}

LinearGradient topinverse() {
  return LinearGradient(
    colors: [
      const Color(0xFF3366FF),
      const Color(0xFF00CCFF),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    // stops: [0.0, 1.0],
    // tileMode: TileMode.clamp,
  );
}

LinearGradient inverse() {
  return LinearGradient(
    colors: [
      const Color(0xFF3366FF),
      const Color(0xFF00CCFF),
    ],
    begin: const FractionalOffset(1.0, 0.0),
    end: const FractionalOffset(0.0, 0.0),
    stops: [0.0, 1.0],
    tileMode: TileMode.clamp,
  );
}

LinearGradient more() {
  return LinearGradient(
    colors: [
      const Color(0xFF28388F),
      const Color(0xFF005BAB),
      // const Color(0xFF0079B8),
      const Color(0xFF005BAB),

      const Color(0xFF28388F),

      // const Color(0xFF0095B6),
      // const Color(0xFF00AEAE),
      // const Color(0xFF845EC2),
    ],
    begin: Alignment.bottomRight,
    end: Alignment.topLeft,

    // begin: const FractionalOffset(1.0, 0.0),
    // end: const FractionalOffset(0.0, 0.0),
    // stops: [0.0, 1.0],
    // tileMode: TileMode.clamp,
  );
}

LinearGradient left() {
  return LinearGradient(
    colors: [
      const Color(0xFFF9F871),
      const Color(0xFFFFC75F),
      const Color(0xFFFF9671),
      const Color(0xFFFF6F91),
      const Color(0xFFD65DB1),
      const Color(0xFF845EC2),
    ],
    begin: Alignment.centerRight,
    end: Alignment.centerRight,

    // begin: const FractionalOffset(1.0, 0.0),
    // end: const FractionalOffset(0.0, 0.0),
    // stops: [0.0, 1.0],
    // tileMode: TileMode.clamp,
  );
}
