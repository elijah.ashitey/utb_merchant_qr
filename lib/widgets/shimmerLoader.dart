import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class HomeLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: double.infinity,
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
              Expanded(
                child: Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: ListView.builder(
                    itemBuilder: (_, __) => Padding(
                      padding: const EdgeInsets.only(bottom: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle),
                                  width: 35.0,
                                  height: 35.0,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  color: Colors.white,
                                  height: 10,
                                  width: double.infinity,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  color: Colors.white,
                                  height: 10,
                                  width: double.infinity,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                              ],
                            ),
                          ),
                          // Container(
                          //   width: 48.0,
                          //   height: 48.0,
                          //   color: Colors.white,
                          // ),
                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                          ),
                          Expanded(
                              flex: 4,
                              child: Stack(
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        color: Colors.white,
                                        height: 10,
                                        width: double.infinity,
                                      ),
                                      const Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 2.0),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          color: Colors.white,
                                          height: 10,
                                          width: double.infinity,
                                        ),
                                      ),
                                      const Padding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 2.0),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(),
                                          ),
                                          Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.3,
                                            decoration: BoxDecoration(
                                              color: Colors.redAccent,
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(15)),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  // Align(
                                  //   alignment: Alignment.bottomLeft,
                                  //   child: Container(
                                  //     width: 48.0,
                                  //     height: 48.0,
                                  //     color: Colors.white,
                                  //   ),
                                  // ),
                                ],
                              ))
                        ],
                      ),
                    ),
                    itemCount: 6,
                  ),
                ),
              )
            ])));
  }
}
