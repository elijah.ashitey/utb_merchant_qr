import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/resources/pinSetup.dart';
import 'package:qr_approval/widgets/Animation/fadeAnimation.dart';
import 'package:qr_approval/widgets/dialogs.dart';
import 'package:qr_approval/widgets/loading.dart';
import 'package:qr_approval/widgets/networkSensitivity.dart';

class ChangePin extends StatefulWidget {
  @override
  _ChangePinState createState() => _ChangePinState();
}

class _ChangePinState extends State<ChangePin> {
  TextEditingController pin1 = TextEditingController();
  TextEditingController pin2 = TextEditingController();
  String oldPin;
  bool hasError = false;
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final def = Provider.of<Default>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Change Pin",
          // style: Theme.of(context).textTheme.title,
        ),
        centerTitle: true,
      ),
      body: NetworkSensitive(
        child: KeyboardAvoider(
          autoScroll: true,
          child: Container(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FadeAnimation(
                    5,
                    Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: CircleAvatar(
                        radius: 40,
                        backgroundColor: Colors.grey[400],
                        child: CircleAvatar(
                          backgroundColor: Colors.grey[100],
                          // foregroundColor: Colors.black,
                          radius: 38,
                          child: Icon(
                            FontAwesomeIcons.key,
                            // color: Colors.blue,
                            color: Colors.grey[400],

                            // color: Color(0xff2323eb),
                            size: 32.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FadeAnimation(
                    1.5,
                    Row(
                      children: <Widget>[
                        Text(
                          "Current pin:",
                          style: TextStyle(
                              // color: Color.fromRGBO(52, 8, 11, 1),
                              ),
                        ),
                        Expanded(
                          child: Container(
                            child: Padding(
                              padding: EdgeInsets.all(0.0),
                              child: PinEntryTextField(
                                showFieldAsBox: false,

                                onSubmit: (pin) {
                                  oldPin = pin;
                                  if (pin != def.getPin) {
                                    err(context, "Wrong Pin");
                                    setState(() {
                                      hasError = false;
                                    });
                                  }
                                },
                                /*(String pin) {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text("Pin"),
                                      content: Text('Pin entered is $pin'),
                                    );
                                  }); //end showDialog()
                            },*/ // end onSubmit
                              ), // end PinEntryTextField()
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.04,
                  ),
                  FadeAnimation(
                    1.7,
                    Text(
                      "Enter new pin",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          // color: Colors.blue,
                          ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  FadeAnimation(
                    2,
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: PinCodeTextField(
                        onChanged: (value) {
                          if (pin1.text == pin2.text) {
                            setState(() {
                              hasError = true;
                            });
                          } else {
                            setState(() {
                              hasError = false;
                            });
                          }
                        },
                        inactiveColor: Colors.grey,
                        backgroundColor: Colors.white.withOpacity(0.1),
                        textInputType: TextInputType.number,
                        length: 4,
                        obsecureText: true,
                        animationType: AnimationType.slide,
                        shape: PinCodeFieldShape.box,
                        animationDuration: Duration(milliseconds: 300),
                        borderRadius: BorderRadius.circular(8),
                        controller: pin1,
                        fieldHeight: 50,
                        fieldWidth: 60,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  FadeAnimation(
                    2.2,
                    Center(
                        child: Text(
                      "Confirm pin",
                      style: TextStyle(
                          // color: Color.fromRGBO(52, 8, 11, 1),
                          ),
                    )),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  FadeAnimation(
                    2.5,
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: PinCodeTextField(
                        inactiveColor: Colors.grey,
                        onChanged: (value) {
                          if (pin1.text == pin2.text) {
                            setState(() {
                              hasError = true;
                            });
                          } else {
                            setState(() {
                              hasError = false;
                            });
                          }
                        },
                        backgroundColor: Colors.white.withOpacity(0.1),
                        length: 4,
                        // negavtiveText: "this is a negative text",
                        textInputType: TextInputType.number,
                        obsecureText: true,
                        animationType: AnimationType.slide,
                        shape: PinCodeFieldShape.box,
                        animationDuration: Duration(milliseconds: 300),
                        borderRadius: BorderRadius.circular(8),
                        controller: pin2,
                        fieldHeight: 50,
                        fieldWidth: 60,

                        onCompleted: null,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.08,
                  ),
                  FadeAnimation(
                    3,
                    Container(
                        child: def.getIsLoadind
                            ? loading()
                            : hasError
                                ? Center(
                                    child: Container(
//                                  color: Colors.orange,
                                      height: 45,
                                      width: double.maxFinite,
                                      decoration: BoxDecoration(
                                        color: Color(0xff2323eb),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      child: FlatButton(
                                          onPressed: () =>
                                              pinConfig(pin2.text, context),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
//                                          side: BorderSide(
//                                            color: Colors.orange,
//                                          )
                                          ),
                                          child: Text(
                                            "Confirm",
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white),
                                          )),
                                    ),
                                  )
                                : Center(
                                    child: Text("pin doesnt match"),
                                  )),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
