import 'package:flutter/material.dart';

class Ella2 extends StatefulWidget {
  @override
  _Ella2State createState() => _Ella2State();
}

class _Ella2State extends State<Ella2> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    List<String> image = [
      'assets/images/bk.jpg',
      'assets/images/bk1.jpg',
      'assets/images/bk2.jpg',
      'assets/images/bk3.jpg',
      'assets/images/bk4.jpg',
      'assets/images/bk5.jpg',
    ];
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: Colors.purple,
            ),
          ),
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(
            "Makarios Library",
            style: TextStyle(color: Colors.purple),
          ),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.search, color: Colors.purple),
                onPressed: () {})
          ],
        ),
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Container(
                // color: Colors.amber,
                height: height * 0.25,
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Recent Reads"),
                    SizedBox(height: height * 0.01),
                    Expanded(
                        child: Container(
                      // color: Colors.red,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(image[index]),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              width: width * 0.2,
                              // height: height * 0.1,
                            ),
                          );
                        },
                        itemCount: image.length,
                      ),
                    ))
                  ],
                ),
              ),
            ),
            Container(
              // color: Colors.cyan,
              height: height * 0.6,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    // color: Colors.blue[50],
                    height: height * 0.06,
                    child: TabBar(
                      labelColor: Colors.blue,
                      labelStyle: TextStyle(fontSize: 15, fontFamily: 'baloo'),
                      labelPadding: EdgeInsets.only(top: 0),
                      tabs: [
                        Tab(
                          text: "Your library",
                        ),
                        Tab(
                          text: "Favourites",
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: height * 0.01),
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Container(
                      // color: Colors.red,
                      child: TabBarView(
                        children: [
                          yourLibrary(context, image),
                          lib(),
                          // Container(
                          //   color: Colors.red,
                          // ),
                        ],
                      ),
                    ),
                  ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

yourLibrary(context, image) {
  var width = MediaQuery.of(context).size.width;
  var height = MediaQuery.of(context).size.height;
  return GridView.count(
    // Create a grid with 2 columns. If you change the scrollDirection to
    // horizontal, this produces 2 rows.
    crossAxisCount: 3,

    // Generate 100 widgets that display their index in the List.
    children: List.generate(image.length, (index) {
      return Image.asset(
        image[index],
        width: 800,
        height: 800,
        fit: BoxFit.cover,
      );
    }),
  );
}

lib() {
  return GridView.builder(
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 3,
    ),
    itemBuilder: (context, index) {
      return Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
          color: Colors.grey,
          width: MediaQuery.of(context).size.width * 0.2,
          height: MediaQuery.of(context).size.height * 0.2,
        ),
      );
    },
  );
}
