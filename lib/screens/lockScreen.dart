import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/provider/Defaults.dart';
// import 'package:qr_approval/widgets/lockScreen.dart';
import 'package:flutter_screen_lock/lock_screen.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:local_auth/error_codes.dart' as auth_error;
// import 'package:qr_approval/widgets/lockScreen.dart';
import 'package:qr_approval/Routes/routeConstants.dart';

class LockScreenPage extends StatefulWidget {
  @override
  _LockScreenPageState createState() => _LockScreenPageState();
}

class _LockScreenPageState extends State<LockScreenPage> {
  @override
  void initState() {
    super.initState();
    //
  }

  @override
  Widget build(BuildContext context) {
    // return lockScreen(context);
    // Future<String> pin() async {
    //   final SharedPreferences sharedPrefs =
    //       await SharedPreferences.getInstance();
    //   String savedpin = sharedPrefs.getString("pin");
    //   return savedpin;
    // }
    final def = Provider.of<Default>(context);

    return LockScreen(
      // context: context,
      // cancelText: "cancel",
      deleteText: "delete",
      canCancel: false,
      correctString: def.getPin,
      canBiometric: true,
      showBiometricFirst: true,
      biometricFunction: (context) async {
        // var didAuthenticate = await authenticate();
        var localAuth = LocalAuthentication();
        // var didAuthenticate = await localAuth.authenticateWithBiometrics(
        //   localizedReason: 'Please authenticate to use app',
        //   stickyAuth: false,
        //   useErrorDialogs: true,
        // );

        // if (didAuthenticate) {
        //   // timeout.setTimeOut(false);
        //   Navigator.of(context).pop();
        // }
        try {
          bool didAuthenticate = await localAuth.authenticateWithBiometrics(
            localizedReason: 'Please authenticate to use app',
            stickyAuth: false,
            useErrorDialogs: true,
          );
          if (didAuthenticate) {
            // timeout.setTimeOut(false);
            Navigator.of(context).pop();
          }
        } on PlatformException catch (e) {
          if (e.code == auth_error.notAvailable) {
            // Handle this exception here.
          }
        }
      },
    );
  }
}
