// import 'dart:math';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/services.dart';
import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/models/historyModel.dart';
import 'package:qr_approval/provider/hitory.dart';
// import 'package:qr_approval/provider/pending.dart';
import 'package:qr_approval/resources/amtformat.dart';
import 'package:qr_approval/resources/apiCalls.dart';
import 'package:qr_approval/screens/pending.dart';
import 'package:qr_approval/widgets/allHistoryLoading.dart';
import 'package:qr_approval/widgets/networkSensitivity.dart';
import 'package:qr_approval/widgets/pedingList.dart';
import 'package:qr_approval/widgets/shimmerLoader.dart';
// import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class AllHistory extends StatefulWidget {
  final String id;
  AllHistory(this.id);
  @override
  _AllHistoryState createState() => _AllHistoryState();
}

class _AllHistoryState extends State<AllHistory> {
  Future<HistoryModel> data;

  DateTime _date = DateTime.now();

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1930),
      lastDate: DateTime(2100),
    );
    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
        print(_date.toString());
      });
    }
  }

  String newdateFormat(DateTime date) {
    return DateFormat("dd-MMM-yyyy").format(date);
    // return DateFormat.yMMMd().format(_date);
  }

  Future<void> refresh() async {
    setState(() {
      data = history(widget.id, context);
    });
  }

  @override
  void initState() {
    super.initState();
    data = history(widget.id, context);
  }

  final SearchBarController<Datum> _searchBarController = SearchBarController();
  bool isReplay = false;
  List<Datum> trans = [];

  Future<List<Datum>> _getALlTransactions(String text) async {
    await Future.delayed(Duration(seconds: text.length == 1 ? 10 : 1));
    List<Datum> searchtrans = [];
    // final transData = Provider.of<Data>(context, listen: false);

    for (var i = 0; i < trans.length; i++) {
      if (
          // trans[i].dbAccName == text
          trans[i].amount.contains(RegExp(text, caseSensitive: false)) ||
              trans[i].transId.contains(RegExp(text, caseSensitive: false)) ||
              trans[i].dbAccName.contains(RegExp(text, caseSensitive: false))) {
        searchtrans.add(trans[i]);
      }
    }
    return searchtrans;
  }

  @override
  Widget build(BuildContext context) {
    final transData = Provider.of<HistoryData>(context);

    return Scaffold(
      floatingActionButton: GestureDetector(
        onTap: () => refresh(),
        child: CircleAvatar(
          child: Icon(
            Icons.refresh,
            color: Colors.white,
          ),
        ),
      ),
      body: NetworkSensitive(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bk1.jpg"), fit: BoxFit.cover),
          ),
          child: Container(
            color: Colors.black26,
            child: Column(
              children: <Widget>[
                /*Container(
                  decoration: BoxDecoration(
                    // borderRadius: BorderRadius.only(
                    //   bottomLeft: Radius.circular(15),
                    //   bottomRight: Radius.circular(15),
                    // ),
                    color: Colors.blue[50],
                  ),
                  // height: 40,
                  child: Row(children: <Widget>[
                    Expanded(
                      child: ListTile(
                        onTap: () {
                          selectDate(context);
                          // stopCh.setDate(dateFormat(_date));
                        },
                        title: Text(
                          "Select Date",
                          style: TextStyle(color: Colors.blue),
                        ),
                        subtitle: Text(dateFormat(_date)),
                        trailing: Icon(
                          Icons.date_range,
                          // color: _iconColor,
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width * 0.3,
                      // width: double.maxFinite,
                      decoration: BoxDecoration(
                        // gradient: getCustomgradient(),
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                      ),
                      child: FlatButton(
                        onPressed: () {},
                        child: Text('Search',
                            style:
                                TextStyle(color: Colors.blue, fontSize: 18.0)),
                        // textColor: Colors.red,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Colors.blue,
                                width: 1,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(6)),
                      ),
                    ),
                    SizedBox(width: 15),
                  ]),
                ),*/
                Expanded(
                  child: Container(
                    child: FutureBuilder(
                        future: data,
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.connectionState ==
                                  ConnectionState.none ||
                              snapshot.connectionState ==
                                  ConnectionState.active ||
                              snapshot.connectionState ==
                                  ConnectionState.waiting) {
                            return Center(
                              child: AllHistoryLoading(),
                            );
                          } else if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasError) {
                              final error = snapshot.error as PlatformException;
                              return Center(child: Text(error.message));
                            } else if (snapshot.data == null) {
                              return Center(
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                    Text(
                                      "Error Loading Data",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    RaisedButton(
                                      color: Colors.blue,
                                      onPressed: () => refresh(),
                                      child: Text(
                                        "Retry",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    )
                                  ]));
                            } else if (snapshot.data != null) {
                              HistoryModel res = snapshot.data;
                              // user.setPending(res);

                              if (res.responseCode == "000") {
                                transData.setRejected(res.data);
                                if (res.data.length < 1) {
                                  return Center(
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                        Text(
                                          "No transaction history",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        RaisedButton(
                                          color: Colors.blue,
                                          onPressed: () => refresh,
                                          child: Text(
                                            "Retry",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        )
                                      ]));
                                } else {
                                  trans = res.data;
                                  return Container(
                                    color: Colors.white,
                                    child: SearchBar<Datum>(
                                      shrinkWrap: true,

                                      hintText:
                                          "Search by name, amount or transID",
                                      iconActiveColor: Colors.blue,
                                      loader: HomeLoading(),
                                      icon: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Icon(Icons.search),
                                      ),
                                      searchBarStyle: SearchBarStyle(
                                        padding: EdgeInsets.zero,
                                        backgroundColor: Colors.blue[50],
                                        borderRadius:
                                            BorderRadius.circular(200),
                                      ),
                                      searchBarPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      headerPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      listPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      onSearch: _getALlTransactions,
                                      searchBarController: _searchBarController,
                                      placeHolder: RefreshIndicator(
                                        onRefresh: refresh,
                                        child: ListView.builder(
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            String currency;
                                            if (res.data[index].currency !=
                                                null) {
                                              currency =
                                                  res.data[index].currency;
                                            } else {
                                              currency = " ";
                                            }

                                            if (res.data[index]
                                                        .statusFlag ==
                                                    "N" ||
                                                res.data[index].statusFlag ==
                                                    "Y" ||
                                                res.data[index].statusFlag ==
                                                    "P") {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: GestureDetector(
                                                  onTap: () {
                                                    transData.setData(
                                                        res.data[index]);
                                                    print("Elijah tapped");
                                                    Navigator.pushNamed(context,
                                                        TransHistoryRoute,
                                                        arguments:
                                                            res.data[index]);
                                                  },
                                                  child: Card(
                                                      elevation: 12,
                                                      color: Colors.transparent,
                                                      child: pendingShimmer2(
                                                          context,
                                                          dateFormat(res
                                                              .data[index]
                                                              .postingDate),
                                                          time(res.data[index]
                                                              .postingDate),
                                                          res.data[index]
                                                              .dbAccName,
                                                          res.data[index]
                                                              .transId,
                                                          // "elijah",
                                                          currency +
                                                              " " +
                                                              formatAmounts(
                                                                res.data[index]
                                                                    .amount,
                                                              ),
                                                          flag: res.data[index]
                                                              .statusFlag)),
                                                ),
                                              );
                                            } else {
                                              return Container();
                                            }
                                          },
                                          itemCount: res.data.length,
                                        ),
                                      ),
                                      cancellationWidget: Text("Cancel"),
                                      emptyWidget: Center(
                                          child: Text(
                                        "No item found",
                                        style: TextStyle(color: Colors.black),
                                      )),
                                      // indexedScaledTileBuilder: (int index) =>
                                      //     ScaledTile.count(0, 0),
                                      /*      header: Row(
                                        children: <Widget>[
                                          RaisedButton(
                                            child: Text("sort"),
                                            onPressed: () {
                                              _searchBarController
                                                  .sortList((Post a, Post b) {
                                                return a.body.compareTo(b.body);
                                              });
                                            },
                                          ),
                                          RaisedButton(
                                            child: Text("Desort"),
                                            onPressed: () {
                                              _searchBarController.removeSort();
                                            },
                                          ),
                                          RaisedButton(
                                            child: Text("Replay"),
                                            onPressed: () {
                                              isReplay = !isReplay;
                                              _searchBarController.replayLastSearch();
                                            },
                                          ),
                                        ],
                                      ),*/
                                      onCancelled: () {
                                        print("Cancelled triggered");
                                      },
                                      mainAxisSpacing: 10,
                                      crossAxisSpacing: 10,
                                      crossAxisCount: 1,
                                      onItemFound: (val, index) {
                                        // return Text("workin");
                                        //  String currency;
                                        //     if (val.currency != null) {
                                        //       currency = res.data[index].currency;
                                        //     } else {
                                        //       currency = " ";
                                        //     }
                                        if (res.data[index].statusFlag == "N" ||
                                            res.data[index].statusFlag == "Y" ||
                                            res.data[index].statusFlag == "P") {
                                          return Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: GestureDetector(
                                              onTap: () {
                                                transData.setData(val);
                                                Navigator.pushNamed(
                                                    context, TransHistoryRoute,
                                                    arguments: val);
                                              },
                                              child: Card(
                                                elevation: 12,
                                                color: Colors.transparent,
                                                child: pendingShimmer2(
                                                  context,
                                                  dateFormat(val.postingDate),
                                                  time(val.postingDate),
                                                  val.dbAccName,
                                                  val.transId,
                                                  val.currency +
                                                      " " +
                                                      formatAmounts(val.amount),
                                                  flag: res
                                                      .data[index].statusFlag,
                                                ),
                                              ),
                                            ),
                                          );
                                        } else {
                                          return Container();
                                        }
                                      },
                                    ),
                                  );
                                }
                              } else {
                                return Center(
                                  child: res.message == null
                                      ? Text("Couldnt get Data")
                                      : Text(res.message),
                                );
                              }
                            } else {
                              return Center(
                                child: Text("error from no where"),
                              );
                            }
                          } else {
                            return Center(
                              child: Text("without snapshot error"),
                            );
                          }
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Detail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            Text("Detail"),
          ],
        ),
      ),
    );
  }
}
