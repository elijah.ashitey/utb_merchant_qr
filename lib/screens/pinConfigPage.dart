// import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:qr_approval/resources/pinSetup.dart';
// import 'package:qr_approval/resources/sharedPrefrences.dart';

class PinConfigScreen extends StatefulWidget {
  @override
  _PinConfigScreenState createState() => _PinConfigScreenState();
}

class _PinConfigScreenState extends State<PinConfigScreen> {
  TextEditingController pin1 = TextEditingController();
  TextEditingController pin2 = TextEditingController();
  bool _isLoading = false;
  String p1;
  String p2;
  String thisText = "";
  int pinLength = 4;

  bool hasError = false;
  String errorMessage;
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final res = Provider.of<UserInfo>(context);
    // final _phoneInfo = Provider.of<PhoneInfo>(context);

    return Material(
      child: Stack(
        children: <Widget>[
          Container(
            // color: Color.fromRGBO(60, 162, 151, 1),
            width: MediaQuery.of(context).size.width,
            // height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage("assets/images/bk7.jpg"),
                    fit: BoxFit.cover)),
          ),
          Container(
            // top: MediaQuery.of(context).size.height / 4,
            // bottom: 0,
//               alignment: Alignment.bottomCenter,
            child: KeyboardAvoider(
              autoScroll: true,
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.8),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    )),
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: EdgeInsets.only(left: 28.0, right: 28.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.08,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Image.asset(
                          "assets/images/logo2.png",
                          // "assets/images/logo2.png",
                          width: MediaQuery.of(context).size.width * 0.5,
                          height: MediaQuery.of(context).size.height * 0.1,
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Center(
                          child: Text(
                        "Security Pin Code Setup",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            // color: Color.fromRGBO(52, 8, 11, 1),
                            fontSize: 18),
                      )),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      Text(
                        "This is the 4-digit secret PIN that will be used to secure your app",
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.05,
                      ),
                      Center(
                          child: Text(
                        "Enter pin",
                        style: TextStyle(
                            // color: Color.fromRGBO(52, 8, 11, 1),
                            fontWeight: FontWeight.bold),
                      )),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: PinCodeTextField(
                          onChanged: (value) {
                            if (pin1.text == pin2.text) {
                              setState(() {
                                hasError = true;
                              });
                            } else {
                              setState(() {
                                hasError = false;
                              });
                            }
                          },
                          backgroundColor: Colors.white.withOpacity(0.1),
                          textInputType: TextInputType.number,
                          length: 4,
                          obsecureText: true,
                          animationType: AnimationType.slide,
                          shape: PinCodeFieldShape.box,
                          animationDuration: Duration(milliseconds: 300),
                          borderRadius: BorderRadius.circular(8),
                          controller: pin1,
                          fieldHeight: 50,
                          fieldWidth: 60,
//                            onChanged: (value) {
//                              setState(() {
//                                p1 = value;
//                                print(p1);
//                              });
//                            },
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      Center(
                          child: Text(
                        "Confirm pin",
                        style: TextStyle(
                            // color: Color.fromRGBO(52, 8, 11, 1),
                            fontWeight: FontWeight.bold),
                      )),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: PinCodeTextField(
                          onChanged: (value) {
                            if (pin1.text == pin2.text) {
                              setState(() {
                                hasError = true;
                              });
                            } else {
                              setState(() {
                                hasError = false;
                              });
                            }
                          },
                          backgroundColor: Colors.white.withOpacity(0.1),
                          length: 4,
                          // negavtiveText: "this is a negative text",
                          textInputType: TextInputType.number,
                          obsecureText: true,
                          animationType: AnimationType.slide,
                          shape: PinCodeFieldShape.box,
                          animationDuration: Duration(milliseconds: 300),
                          borderRadius: BorderRadius.circular(8),
                          controller: pin2,
                          fieldHeight: 50,
                          fieldWidth: 60,
                          // enableActiveFill: true,
                          onCompleted: null,
                          /*  (value) {
                            if (pin1.text == pin2.text) {
                              setState(() {
                                hasError = true;
                              });
                            } else {
                              setState(() {
                                hasError = false;
                              });
                            }
                          },*/
//
//                          },
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.08,
                      ),
                      _isLoading
                          ? CircularProgressIndicator()
                          : hasError
                              ? Center(
                                  child: Container(
//                                  color: Colors.orange,
                                    height: 45,
                                    width: double.maxFinite,
                                    decoration: BoxDecoration(
                                      color: Color(0xff2323eb),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                    ),
                                    child: FlatButton(
                                        onPressed: () =>
                                            pinConfig(pin2.text, context),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
//                                          side: BorderSide(
//                                            color: Colors.orange,
//                                          )
                                        ),
                                        child: Text(
                                          "Confirm",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.white),
                                        )),
                                  ),
                                )
                              : Center(
                                  child: Text("pin doesnt match"),
                                )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
