import 'package:flutter/material.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/provider/rejectProv.dart';
// import 'package:approval/resources/provider.dart';

class RejectCommentPage extends StatefulWidget {
  @override
  _RejectCommentPageState createState() => _RejectCommentPageState();
}

class _RejectCommentPageState extends State<RejectCommentPage> {
  bool commentbox = false;

  @override
  Widget build(BuildContext context) {
    // final rejectreasonData = Provider.of<rejectreasonData>(context);
    final rejectreason = Provider.of<RejectReasonProvider>(context);
    TextEditingController comment = TextEditingController();
    // bool _other = false;
    return Scaffold(
      appBar: AppBar(
        title: Text("Comment"),
        centerTitle: true,
        elevation: 0,
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/bk1.jpg"), fit: BoxFit.cover),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              // flex: 3,
              child: Container(
                  color: Colors.black26,
                  child: ListView.builder(
                    itemBuilder: (BuildContext context, int index) {
                      if (rejectreason.getCode ==
                          rejectreasonData[index].code) {
                        return Padding(
                          padding:
                              EdgeInsets.only(left: 8.0, right: 8, bottom: 5),
                          child: Card(
                            // color: Colors.blue[100],
                            child: ListTile(
                              onTap: () {
                                rejectreason
                                    .setCode(rejectreasonData[index].code);

                                if (rejectreason.getCode == 00) {
                                  setState(() {
                                    commentbox = true;
                                    print("display comment box");
                                  });
                                } else {
                                  setState(() {
                                    commentbox = false;
                                  });
                                  rejectreason.setComment(
                                      rejectreasonData[index].reject);
                                  rejectreason
                                      .setCode(rejectreasonData[index].code);

                                  Navigator.pop(
                                      context, rejectreasonData[index].reject);
                                }
                              },
                              leading: CircleAvatar(
                                backgroundColor: Colors.blue[50],
                                child: Icon(Icons.chat),
                              ),
                              title: Text(rejectreasonData[index].reject),
                              trailing: Icon(
                                Icons.check,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        );
                      } else {
                        return Padding(
                          padding:
                              EdgeInsets.only(left: 8.0, right: 8, bottom: 5),
                          child: Card(
                            child: ListTile(
                              onTap: () {
                                rejectreason
                                    .setCode(rejectreasonData[index].code);

                                if (rejectreason.getCode == 00) {
                                  setState(() {
                                    commentbox = true;
                                    print("display comment box $commentbox");
                                  });
                                } else {
                                  setState(() {
                                    commentbox = false;
                                  });
                                  rejectreason.setComment(
                                      rejectreasonData[index].reject);

                                  Navigator.pop(
                                      context, rejectreasonData[index].reject);
                                }
                              },
                              leading: CircleAvatar(
                                backgroundColor: Colors.blue[50],
                                child: Icon(Icons.chat),
                              ),
                              title: Text(rejectreasonData[index].reject),
                            ),
                          ),
                        );
                      }
                    },
                    itemCount: rejectreasonData.length,
                  )),
            ),
            commentbox
                ? KeyboardAvoider(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                        color: Colors.blue[50],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: Container(
                                    child: TextField(
                              maxLines: 3,
                              decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                  width: 2.0,
                                  color: Colors.blue,
                                )),
                                labelText: 'comment here',
                              ),
                              controller: comment,
                            ))),
                            GestureDetector(
                                onTap: () {
                                  if (comment.text.isEmpty) {
                                  } else {
                                    rejectreason.setComment(comment.text);
                                    rejectreason.setCode(00);
                                    // RejectReason result =
                                    //     RejectReason(comment.text, 00);

                                    Navigator.pop(context, comment.text);
                                  }
                                },
                                child: CircleAvatar(
                                  child: Icon(Icons.send),
                                ))
                          ],
                        ),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
