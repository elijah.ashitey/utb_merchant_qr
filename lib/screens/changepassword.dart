import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/resources/changePassword.dart';
import 'package:qr_approval/widgets/Animation/fadeAnimation.dart';
import 'package:qr_approval/widgets/gradient.dart';
import 'package:qr_approval/widgets/loading.dart';
import 'package:qr_approval/widgets/networkSensitivity.dart';

//import 'package:local_auth/local_auth.dart';
class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController pass1 = TextEditingController();
  TextEditingController pass2 = TextEditingController();
  TextEditingController ans = TextEditingController();
  TextEditingController old = TextEditingController();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final def = Provider.of<Default>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Change Password",
          // style: Theme.of(context).textTheme.title,
        ),
        centerTitle: true,
      ),
      body: NetworkSensitive(
        child: KeyboardAvoider(
          autoScroll: true,
          child: Container(
            // color: Colors.black54,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bk1.jpg"),
                  fit: BoxFit.cover),
            ),
            child: Container(
              color: Colors.white.withOpacity(0.9),
              child: Padding(
                padding: const EdgeInsets.only(left: 28.0, right: 28.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        FadeAnimation(
                          5,
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                            child: CircleAvatar(
                              radius: 40,
                              backgroundColor: Colors.grey[400],
                              child: CircleAvatar(
                                backgroundColor: Colors.grey[100],
                                // foregroundColor: Colors.black,
                                radius: 38,
                                child: Icon(
                                  FontAwesomeIcons.key,
                                  // color: Colors.blue,
                                  color: Colors.grey[400],

                                  // color: Color(0xff2323eb),
                                  size: 32.0,
                                ),
                              ),
                            ),
                          ),
                        ),
                        // CircleAvatar()
                        SizedBox(
                          height: 20,
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              FadeAnimation(
                                1,
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Colors.grey[100]))),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'please enter current password';
                                      }
                                      return null;
                                    },
                                    obscureText: true,
                                    decoration: InputDecoration(
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                          width: 2.0,
                                          color: Colors.blue,
                                        )),
                                        labelText: "Current Password ",
                                        hintStyle:
                                            TextStyle(color: Colors.grey[400])),
                                    controller: old,
                                  ),
                                ),
                              ),
                              FadeAnimation(
                                1.5,
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'please enter new password';
                                      }
                                      return null;
                                    },
                                    controller: pass1,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 2.0, color: Colors.blue)),
                                      // suffixIcon: IconButton(
                                      //   color:
                                      //       Theme.of(context).primaryColor,
                                      //   onPressed: _toggleVisibility,
                                      //   icon: _isHidden
                                      //       ? Icon(Icons.visibility_off)
                                      //       : Icon(Icons.visibility),
                                      // ),
                                      labelText: "New Password",
                                    ),
                                  ),
                                ),
                              ),
                              FadeAnimation(
                                2,
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'please enter new password';
                                      }
                                      if (value != pass1.text) {
                                        return 'passwords do not match';
                                      }
                                      return null;
                                    },
                                    controller: pass2,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              width: 2.0, color: Colors.blue)),
                                      // suffixIcon: IconButton(
                                      //   color:
                                      //       Theme.of(context).primaryColor,
                                      //   onPressed: _toggleVisibility,
                                      //   icon: _isHidden
                                      //       ? Icon(Icons.visibility_off)
                                      //       : Icon(Icons.visibility),
                                      // ),
                                      labelText: "Confirm Password",
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    FadeAnimation(
                      2.5,
                      Center(
                        child: def.getIsLoadind
                            ? loading()
                            : Container(
                                // color: Colors.blue,
                                height: 45,
                                width: double.maxFinite,
                                decoration: BoxDecoration(
                                  // color: Color.fromRGBO(244, 128, 71, 1),
                                  // color: Colors.blue,
                                  gradient: inverse(),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                ),
                                child: FlatButton(
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        changePassword(
                                            old.text, pass1.text, context);
                                      }
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
//                                          side: BorderSide(
//                                            color: Colors.orange,
//                                          )
                                    ),
                                    child: Text(
                                      "Change Password",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    )),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
