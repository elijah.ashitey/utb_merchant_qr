import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/provider/User.dart';
import 'package:qr_approval/provider/pending.dart';
import 'package:qr_approval/resources/amtformat.dart';
import 'package:qr_approval/screens/pending.dart';
// import 'package:qr_approval/widgets/gradient.dart';
import 'package:qr_approval/screens/approveScreen.dart';

class SuccessPage extends StatefulWidget {
  final String message;
  SuccessPage(this.message);
  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  Widget build(BuildContext context) {
    final transData = Provider.of<Data>(context);
    final user = Provider.of<UserInfo>(context);
    String currency;
    if (transData.getData.currency != null) {
      currency = transData.getData.currency;
    } else {
      currency = " ";
    }
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.lightBlueAccent,
        body: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.5,
                width: double.infinity,
                // color: Colors.red,
                child: Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                  child: Column(
                    children: <Widget>[
                      Spacer(),
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 40,
                        child: Icon(
                          Icons.check,
                          size: 70,
                          color: Colors.green,
                        ),
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          widget.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 30),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            // gradient: top(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 1.0, right: 1),
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                            // topLeft: Radius.lerp(
                            //     Radius.circular(40), Radius.circular(60), 0.5),
                            topRight: Radius.circular(70),
                            topLeft: Radius.circular(70),
                          ),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.6,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              // gradient: top(),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0, top: 15, bottom: 15, right: 5),
                              child: Column(
                                // crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Transaction Details",
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Divider(),
                                  info2("Name", transData.getData.dbAccName,
                                      context),
                                  info2(
                                      "Amount",
                                      currency +
                                          " " +
                                          formatAmounts(
                                              transData.getData.amount),
                                      context),
                                  info2(
                                      "Date",
                                      dateFormat(transData.getData.postingDate),
                                      context),
                                  info2(
                                      "Time",
                                      time(transData.getData.postingDate),
                                      context),
                                  info2("Approved by",
                                      user.getUser.data.fullName, context),
                                  Spacer(),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Container(
                                      height: 50,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          // gradient: more(),
                                          // color: Colors.white,
                                          color: Colors.lightBlueAccent
                                              .withOpacity(1)),
                                      child: FlatButton(
                                        onPressed: () =>
                                            Navigator.pushNamedAndRemoveUntil(
                                          context,
                                          HomePageRoute,
                                          ModalRoute.withName("/"),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Back to Home",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
