import 'package:flutter/material.dart';

import 'allTransactions.dart';
import 'historyPaage.dart';

class HistoryScreen extends StatefulWidget {
  final String id;
  HistoryScreen(this.id);
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          child: Container(
            color: Colors.blue[50],
            height: 35.0,
            child: TabBar(
              labelColor: Colors.blue,
              labelStyle: TextStyle(fontSize: 15, fontFamily: 'baloo'),
              labelPadding: EdgeInsets.only(top: 0),
              tabs: [
                Tab(
                  text: "Today",
                ),
                Tab(
                  text: "All",
                ),
                // Tab(icon: Icon(Icons.directions_car)),
                // Tab(icon: Icon(Icons.directions_transit)),
                // Tab(icon: Icon(Icons.directions_bike)),
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: [
            History(widget.id),
            AllHistory(widget.id),
          ],
        ),
      ),
    );
  }
}
