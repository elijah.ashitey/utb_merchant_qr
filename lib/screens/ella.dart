import 'package:flutter/material.dart';

class Ella extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bookshelf.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                gradient: top(),
              ),
            ),
            Positioned(
                bottom: 50,
                top: 250,
                left: 15,
                right: 12,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // SizedBox(width: 75),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(50, 0, 0, 10),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          'Hi there,',
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                            fontSize: 35,
                            // fontFamily: "Montserrat-Bold"
                          ),
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          'Welcome to the\nDag Heward-Mills\nDigital Makarios Library',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),

                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "Click 'Get Started' to begin your\njourney to Spiritual Growth.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.white,
                              fontSize: 15),
                        ),
                      ),
                    ),

                    Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 55.0, vertical: 15.0),
                      width: double.infinity,
                      child: RaisedButton(
                        padding: const EdgeInsets.all(15.0),
                        textColor: Colors.white,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0),
                        ),
                        onPressed: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => OnboardingPage()));
                        },
                        child: Text("Get Started",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 20.0,
                                color: Color(0xff4A014B))),
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Already have an account?',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                              fontSize: 14),
                        ),
                        GestureDetector(
                          onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => LoginPage()));
                          },
                          child: Text(
                            'Log In',
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                                fontSize: 13),
                          ),
                        )
                      ],
                    )
                  ],
                )),
            /*  Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: height * 0.5,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(50, 0, 0, 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Hi there,',
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                            fontSize: 35,
                            // fontFamily: "Montserrat-Bold"
                          ),
                        ),
                        Text(
                          'Welcome to the\nDag Heward-Mills\nDigital Makarios Library',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                            fontSize: 22,
                          ),
                        ),
                      ],
                    ),
                  ),
                )),*/
            // Align(
            //     alignment: Alignment.bottomCenter,
            //     child: Container(
            //       decoration: BoxDecoration(
            //         gradient: top(),
            //       ),
            //       height: height * 0.5,
            //     )),
            // Container(),
          ],
        ),
      ),
    );
  }
}

LinearGradient top() {
  return LinearGradient(
    colors: [
      Colors.black.withOpacity(0.1),
      Colors.black.withOpacity(0.2),
      Colors.black.withOpacity(0.3),
      Colors.black.withOpacity(0.5),
      Colors.purple[300].withOpacity(0.8),
      // Colors.purple[400].withOpacity(0.8),
      Colors.purple[600].withOpacity(0.9),
      Colors.purple[800].withOpacity(0.9),

      // const Color(0xFF00CCFF),
      // const Color(0xFF3366FF),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    // stops: [0.0, 1.0],
    // tileMode: TileMode.clamp,
  );
}
