import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
// import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/resources/login.dart';
import 'package:qr_approval/widgets/Animation/fadeAnimation.dart';
import 'package:qr_approval/widgets/loading.dart';
// import 'package:qr_approval/widgets/gradient.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isHidden = true;
  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  Widget build(BuildContext context) {
    final def = Provider.of<Default>(context);

    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bk7.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Container(
          // decoration: BoxDecoration(color: Color(0xff2323eb).withOpacity(0.7)),
          decoration: BoxDecoration(
            // color: Colors.blue.withOpacity(0.5),
            color: Colors.white.withOpacity(0.8),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.875,
              child: Column(
                children: <Widget>[
                  // Positioned(
                  // top: MediaQuery.of(context).size.height * 0.04,
                  // left: MediaQuery.of(context).size.width * 0.1,
                  // width: MediaQuery.of(context).size.width * 0.8,
                  // height: MediaQuery.of(context).size.height * 0.2,
                  // child:
                  FadeAnimation(
                      1,
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.08,

                            // decoration: BoxDecoration(
                            //     image: DecorationImage(
                            //         image: AssetImage('assets/images/logo.jpg'))),
                            child: Stack(
                              children: <Widget>[
                                Hero(
                                    tag: 'logo',
                                    child:
                                        Image.asset('assets/images/logo2.png')),
                                // Container(
                                //   width: double.infinity,
                                //   height:
                                //       MediaQuery.of(context).size.height * 0.13,
                                //   // color: Colors.lightBlue.withOpacity(0.2),
                                //   // child: Text("Elijahasasasasa"),
                                // ),
                              ],
                            )),
                      )),
                  // ),
                  // Positioned(
                  //   top: MediaQuery.of(context).size.height * 0.3,
                  //   left: MediaQuery.of(context).size.width * 0.4,
                  //   child:
                  FadeAnimation(
                      1.5,
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: 50),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 4.0),
                                child: Icon(
                                  FontAwesomeIcons.qrcode,
                                  // color: Colors.blue,
                                  color: Color(0xff2323eb),

                                  // color: Color(0xff2323eb),
                                  size: 32.0,
                                ),
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Text(
                                "QR Merchant",
                                style: TextStyle(
                                    // color: Colors.blue,
                                    color: Color(0xff2323eb),
                                    // color: Color(0xff2323eb),
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      )),
                  // ),
                  Text(
                    "V-1.0",
                    style: TextStyle(
                        // color: Colors.blue,
                        color: Color(0xff2323eb),
                        // color: Color(0xff2323eb),
                        fontSize: 12,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  Container(
                    child: Padding(
                      padding: EdgeInsets.all(30.0),
                      child: Column(
                        children: <Widget>[
                          FadeAnimation(
                              2,
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.blueGrey,
                                          // Color.fromRGBO(143, 148, 251, .2),
                                          blurRadius: 20.0,
                                          offset: Offset(0, 10))
                                    ]),
                                child: Form(
                                  key: _formKey,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.all(8.0),
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: Colors.grey[100]))),
                                        child: TextFormField(
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'please enter username';
                                            }
                                            return null;
                                          },
                                          decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: "Username ",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[400])),
                                          controller: username,
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(8.0),
                                        child: TextFormField(
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'please enter password';
                                            }
                                            if (value.length < 8) {
                                              return 'password should be more than 8 character';
                                            }
                                            return null;
                                          },
                                          controller: password,
                                          obscureText: _isHidden,
                                          decoration: InputDecoration(
                                              suffixIcon: IconButton(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                onPressed: _toggleVisibility,
                                                icon: _isHidden
                                                    ? Icon(Icons.visibility_off)
                                                    : Icon(Icons.visibility),
                                              ),
                                              border: InputBorder.none,
                                              hintText: "Password",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[400])),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 30,
                          ),
                          FadeAnimation(
                              2.5,
                              def.getIsLoadind
                                  ? loading()
                                  : Container(
                                      height: 50,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        // gradient: more(),
                                        // color: Colors.blue,
                                        color: Color(0xff2323eb),
                                        // color: Color(0xffd3ef41),
                                      ),
                                      child: FlatButton(
                                        onPressed: () {
                                          if (_formKey.currentState
                                              .validate()) {
                                            auth(username.text, password.text,
                                                context);
                                          }
                                        },
                                        child: Center(
                                          child: Text(
                                            "Login",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    )),
                          // Padding(
                          //   padding: const EdgeInsets.all(15.0),
                          //   child: FadeAnimation(
                          //       3,
                          //       Text(
                          //         "Forgot Password?",
                          //         style: TextStyle(color: Colors.white),
                          //       )),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    )
        /* body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bk.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            color: Colors.blue.withOpacity(0.7),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.6,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(50.0),
                            ),
                          ),
                          filled: true,
                          hintStyle: TextStyle(color: Colors.grey[800]),
                          hintText: "Username",
                          fillColor: Colors.white.withOpacity(0.8),
                          prefixIcon: Icon(Icons.person)),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),*/
        );
  }
}
