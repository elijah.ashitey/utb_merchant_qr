// import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/models/pendingModel.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/provider/rejectProv.dart';
import 'package:qr_approval/resources/amtformat.dart';
import 'package:qr_approval/resources/appprove.dart';
import 'package:qr_approval/screens/pending.dart';
import 'package:qr_approval/widgets/dialogs.dart';
import 'package:qr_approval/widgets/funtionWidget.dart';
import 'package:qr_approval/widgets/gradient.dart';
import 'package:qr_approval/widgets/loading.dart';
// import 'package:qr_approval/widgets/gradientAutoSizeText.dart';

class ApproveScreen extends StatefulWidget {
  final Datum data;
  ApproveScreen(this.data);
  @override
  _ApproveScreenState createState() => _ApproveScreenState();
}

class _ApproveScreenState extends State<ApproveScreen> {
  @override
  void initState() {
    super.initState();
    // final rejectreason =
    //     Provider.of<RejectReasonProvider>(context, listen: false);
    // rejectreason.setCode(null);
    // rejectreason.setComment(null);
  }

  String commentReason;
  bool _comment = false;
  _getReason(BuildContext context) async {
    var result = await Navigator.pushNamed(context, RejectCommentRoute);
    if (result != null) {
      setState(() {
        _comment = true;
        commentReason = result;
        print("Comment reason: $commentReason");
        print("Comment reason from pop: $result");
      });
    } else {
      setState(() {
        _comment = false;
      });
    }
  }

  String currency;
  @override
  Widget build(BuildContext context) {
    // final rejectreasonList = Provider.of<RejectReasonList>(context);
    // final rejectresaon = Provider.of<RejectReasonProvider>(context);
    final def = Provider.of<Default>(context);

    if (widget.data.currency == null) {
      currency = " ";
    } else {
      currency = widget.data.currency;
    }
    return SafeArea(
      top: false,
      child: Scaffold(
        // appBar: AppBar(
        //   elevation: 0,
        //   title: Row(
        //     children: <Widget>[
        //       Expanded(
        //         child: CircleAvatar(
        //             backgroundColor: Colors.white,
        //             radius: 15,
        //             child: Icon(
        //               Icons.person,
        //               color: Colors.grey,
        //             )),
        //       ),
        //       Expanded(flex: 4, child: Text("ELijah Ashitey"))
        //     ],
        //   ),
        // ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bk1.jpg"), fit: BoxFit.cover),
          ),
          child: Container(
            color: Colors.black26,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.arrow_back_ios),
                        onPressed: () => Navigator.pop(context)),
                    // IconButton(
                    //     icon: Icon(Icons.close),
                    //     onPressed: () => Navigator.pop(context))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 15.0,
                    right: 15,
                    top: 5.0,
                    bottom: 5.0,
                  ),
                  child: Container(
                    child: Card(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Payment of",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              GradientText(
                                currency,
                                gradient: basic(),
                                style: TextStyle(fontSize: 17),
                              ),
                              SizedBox(
                                width: 3,
                              ),
                              GradientText(
                                formatAmounts(widget.data.amount),
                                // "999,000,000,000,000,000.00",
                                gradient: basic(),
                                style: TextStyle(
                                    fontSize: 40, fontWeight: FontWeight.bold),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Expanded(
                  // flex: 11,
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Card(
                          elevation: 5,
                          // color: Colors.red,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  color: Colors.white.withOpacity(0.7),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5.0, left: 10),
                                    child: ListView(
                                      children: <Widget>[
                                        Text(
                                          "Basic info",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                        info(Icons.person,
                                            widget.data.dbAccName, context),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                                child: Container(
                                              child: info(
                                                  Icons.date_range,
                                                  dateFormat(
                                                      widget.data.postingDate),
                                                  context),
                                            )),
                                            Expanded(
                                                child: Container(
                                              child: info(
                                                  Icons.access_time,
                                                  time(widget.data.postingDate),
                                                  context),
                                            )),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              "Status:",
                                              style: TextStyle(
                                                // color: Colors.lightBlueAccent,
                                                // fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "Pending",
                                              style: TextStyle(
                                                color: Colors.lightBlueAccent,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Divider(),
                                        Text(
                                          "Transaction Details",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                        info2("Transaction ID",
                                            widget.data.transId, context),
                                        info2("Recipient name",
                                            widget.data.crAccName, context),
                                        info2(
                                            "Recipient acc",
                                            accNum(widget.data.crAccNum),
                                            context),
                                        info2(
                                            "Amount",
                                            currency +
                                                " " +
                                                formatAmounts(
                                                    widget.data.amount),
                                            context),
                                        _comment
                                            ? comment(
                                                "Comment",
                                                commentReason,
                                                context,
                                                () => _getReason(context))
                                            : Container(),
                                        // rejectresaon.getComment == null
                                        //     ? Container()
                                        //     : comment(
                                        //         "Comment",
                                        //         rejectresaon.getComment,
                                        //         context, () {
                                        //         Navigator.of(context).pushNamed(
                                        //             RejectCommentRoute);
                                        // }),
                                        /*Container(
                                          child: Consumer<RejectReasonProvider>(
                                              builder:
                                                  (context, _reason, child) {
                                            if (_reason.getComment != null) {
                                              return comment(
                                                  "Comment",
                                                  _reason.getComment,
                                                  context, () {
                                                Navigator.of(context).pushNamed(
                                                    RejectCommentRoute);
                                              });
                                            } else {
                                              return Container();
                                            }
                                          }),
                                        ),*/
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              def.getIsLoadind
                                  ? loading()
                                  : Container(
                                      height: 50,
                                      color: Colors.blue[50],
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 2,
                                            child: Container(
                                              height: double.infinity,
                                              width: double.infinity,
                                              color: Colors.red[400],
                                              child: FlatButton(
                                                child: Text(
                                                  "Reject",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 20),
                                                ),
                                                onPressed: () {
                                                  if (!_comment) {
                                                    _getReason(context);
                                                    // rejectreasonList.setReasons(
                                                    //     rejectreasonData);

                                                    // Navigator.of(context)
                                                    //     .pushNamed(
                                                    //         RejectCommentRoute);
                                                  } else {
                                                    // print("making the call");
                                                    rejectLogic(
                                                        widget.data.transId,
                                                        widget.data.approvedBy,
                                                        commentReason,
                                                        context);
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Container(
                                              height: double.infinity,
                                              width: double.infinity,
                                              color: Colors.green[400],
                                              child: FlatButton(
                                                  child: Text(
                                                    "Accept",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 20),
                                                  ),
                                                  onPressed: () => conf(context,
                                                          "Are you sure want to approve",
                                                          () {
                                                        approveLogic(
                                                            widget.data.transId,
                                                            widget.data
                                                                .approvedBy,
                                                            context);
                                                      })),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                            ],
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

dataDisplay(name, desc) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Text(name),
          ),
          Expanded(
            flex: 3,
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.lightBlueAccent[100],
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(child: Text(desc)),
                )),
          ),
        ],
      ),
    ),
  );
}

info(icon, text, context) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      children: <Widget>[
        Icon(
          icon,
          size: 20,
          color: Colors.grey,
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          text,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    ),
  );
}

info2(desc, text, context) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      // mainAxisAlignment: MainAxisAlignment.center,
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
            flex: 2,
            child: Text(
              desc,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Colors.grey[600]),
            )),
        // SizedBox(
        //   width: 10,
        // ),
        Expanded(
            flex: 3,
            child: Text(
              text,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w300,
                  ),
            )),
      ],
    ),
  );
}

comment(desc, text, context, function) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      children: <Widget>[
        Expanded(
            flex: 2,
            child: Text(
              desc,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Colors.grey[600]),
            )),
        // SizedBox(
        //   width: 10,
        // ),
        Expanded(
            flex: 3,
            child: Container(
                child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    text,
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300,
                        ),
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: Colors.lightBlueAccent,
                  ),
                  onPressed: function,
                )
              ],
            )))
      ],
    ),
  );
}
