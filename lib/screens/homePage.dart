import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:qr_approval/provider/pending.dart';
import 'package:qr_approval/provider/hitory.dart';
// import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/models/pendingModel.dart';
import 'package:qr_approval/provider/User.dart';
// import 'package:qr_approval/resources/amtformat.dart';
// import 'package:qr_approval/resources/apiCalls.dart';
import 'package:qr_approval/resources/constants.dart';
// import 'package:qr_approval/screens/historyPaage.dart';
import 'package:qr_approval/screens/pending.dart';
import 'package:qr_approval/widgets/dialogs.dart';
import 'package:qr_approval/widgets/gradient.dart';

import 'HistoryScreen.dart';
// import 'package:qr_approval/widgets/networkSensitivity.dart';
// import 'package:qr_approval/widgets/pedingList.dart';
// import 'package:qr_approval/widgets/shimmerLoader.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<PendingModel> data;
  String id;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    final user = Provider.of<UserInfo>(context, listen: false);
    id = user.getUser.data.userId;
  }

  String dateFormat(DateTime date) {
    return DateFormat("d/M/y").format(date);
    // return DateFormat.d().add_yMMM().format(date);
  }

  String time(DateTime date) {
    return DateFormat("hh:mm").format(date);
    // return DateFormat.d().add_yMMM().format(date);
  }

  int currentIndex = 0;
  // int _counter = 0;

  Widget _pageChoser(int page) {
    switch (page) {
      case 0:
        return PendingPage(id);
        break;
      case 1:
        return HistoryScreen(id);
        // return History(id);
        break;
      case 2:
        return Container();

      //   break;
      // case 3:
      //   return SettingsScreen();
      //   break;
      default:
        return Container(
            child: Center(
          child: Text("No page found"),
        ));
    }
  }

  Widget title(int page) {
    switch (page) {
      case 0:
        return Text("Pending",
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ));
        break;
      case 1:
        return Text("Transaction History",
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ));
        break;
      case 2:
        return Container();

      //   break;
      // case 3:
      //   return SettingsScreen();
      //   break;
      default:
        return Container(
            child: Center(
          child: Text("No page found"),
        ));
    }
  }

  Color color(int page) {
    switch (page) {
      case 0:
        return Colors.white;
        break;
      case 1:
        return Colors.white;
        break;
      case 2:
        return Colors.blue;

      //   break;
      // case 3:
      //   return SettingsScreen();
      //   break;
      default:
        return Colors.blue;
    }
  }

  Color color2(int page) {
    switch (page) {
      case 0:
        return Colors.blue;
        break;
      case 1:
        return Colors.blue;
        break;
      case 2:
        return Colors.blue;

      //   break;
      // case 3:
      //   return SettingsScreen();
      //   break;
      default:
        return Colors.blue;
    }
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserInfo>(context);
    final history = Provider.of<HistoryData>(context);
    // final pending = Provider.of<Data>(context);

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: Drawer(
          // elevation: ,
          child: Container(
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //       image: AssetImage("assets/images/bk1.jpg"),
            //       fit: BoxFit.cover),
            //   // gradient: topinverse(),
            // ),
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    // image: DecorationImage(
                    //     image: AssetImage("assets/images/bk1.jpg"),
                    //     fit: BoxFit.cover),
                    gradient: topinverse(),
                  ),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.35,
                  // color: Colors.red,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.05,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                              onTap: () => Navigator.pop(context),
                              child: Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 30,
                              )),
                          SizedBox(width: 15),
                        ],
                      ),
                      CircleAvatar(
                        radius: 50,
                        backgroundColor: Colors.white,
                        backgroundImage: NetworkImage(
                          "${Constants.image}" + user.getUser.data.userImage,
                        ),
                        // child: Icon(
                        //   Icons.person,
                        //   size: 50,
                        // ),
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Text(
                          user.getUser.data.fullName,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Text(
                          user.getUser.data.userId,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    // decoration: BoxDecoration(
                    //   image: DecorationImage(
                    //       image: AssetImage("assets/images/bk1.jpg"),
                    //       fit: BoxFit.cover),
                    //   // gradient: topinverse(),
                    // ),
                    child: Container(
                      // color: Colors.black26,
                      child: ListView(
                        padding: EdgeInsets.all(0),
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 8),
                            child: Text(
                              "Summary",
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Divider(
                              height: 16,
                            ),
                          ),
                          drawerItems(
                            Icons.timer,
                            "Pending",
                            Consumer<Data>(builder: (context, _reason, child) {
                              if (_reason.getPending != null) {
                                return Text(_reason.getPending);
                              } else {
                                return Text("");
                              }
                            }),
                          ),
                          drawerItems(
                            Icons.close,
                            "Rejected",
                            Text(history.getRjected ?? ""),
                            // Consumer<HistoryData>(
                            //     builder: (context, _reason, child) {
                            //   if (_reason.getRjected != null) {
                            //     return Text(_reason.getRjected);
                            //   } else {
                            //     return Text("");
                            //   }
                            // }),
                          ),
                          drawerItems(
                            Icons.check, "Approved",
                            Text(history.getGetApproved ?? ""),
                            // Consumer<HistoryData>(
                            //     builder: (context, _reason, child) {
                            //   if (_reason.getRjected != null) {
                            //     return Text(_reason.getRjected);
                            //   } else {
                            //     return Text("");
                            //   }
                            // }),
                          ),
                          /* Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Container(
                              height: 8,
                              width: double.infinity,
                              color: Colors.black,
                            ),
                          ),
                          ListTile(
                            contentPadding: EdgeInsets.only(left: 10),
                            leading: Icon(
                              Icons.timer,
                              color: Colors.lightBlueAccent,
                            ),
                            title: Text("Pending"),
                            trailing: Container(
                              // height: 10,
                              // width: 10,
                              child: Consumer<Data>(
                                  builder: (context, _reason, child) {
                                if (_reason.getPending != null) {
                                  return Text(_reason.getPending);
                                } else {
                                  return Text("");
                                }
                              }),
                            ),

                            //  Text(pending.getPending),
                          ),
                          ListTile(
                            contentPadding: EdgeInsets.only(left: 10),

                            leading: Icon(
                              Icons.close,
                              color: Colors.lightBlueAccent,
                            ),
                            title: Text("Rejected"),
                            trailing: Container(
                              // height: 10,
                              // width: 10,
                              child: Consumer<HistoryData>(
                                  builder: (context, _reason, child) {
                                if (_reason.getRjected != null) {
                                  return Text(_reason.getRjected);
                                } else {
                                  return Text("");
                                }
                              }),
                            ),
                            // Text(history.getRjected ?? "0"),
                          ),
                          ListTile(
                            contentPadding: EdgeInsets.only(left: 10),

                            leading: Icon(
                              Icons.check,
                              color: Colors.lightBlueAccent,
                            ),
                            title: Text("Approved"),
                            trailing: Container(
                              // height: 10,
                              // width: 10,
                              child: Consumer<HistoryData>(
                                  builder: (context, _reason, child) {
                                if (_reason.getRjected != null) {
                                  return Text(_reason.getRjected);
                                } else {
                                  return Text("");
                                }
                              }),
                            ),
                            //  Text(history.getGetApproved ?? "0"),
                          ),*/
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 8),
                            child: Text(
                              "Settings",
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Divider(
                              height: 16,
                            ),
                          ),
                          drawerNav(Icons.vpn_key, "Change pasword", () {
                            Navigator.pop(context);
                            Navigator.pushNamed(context, ChangePasswordRoute);
                          }),
                          drawerNav(FontAwesomeIcons.key, "Change pin", () {
                            Navigator.pop(context);

                            Navigator.pushNamed(context, ChangePinRoute);
                          }),
                          drawerNav(FontAwesomeIcons.qrcode, "View QR", () {
                            Navigator.pop(context);
                            Navigator.pushNamed(context, ViewQRRoute);
                          }),
                        ],
                      ),
                    ),
                  ),
                ),
                // Divider(),
                Container(
                  height: MediaQuery.of(context).size.height * 0.08,
                  color: Colors.blue,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        onTap: () => conf(
                          context,
                          "Do you want to log-out",
                          () => Navigator.pushNamedAndRemoveUntil(
                              context, LoginPageRoute, (route) => false),
                        ),
                        leading: Icon(
                          Icons.power_settings_new,
                          color: Colors.white,
                        ),
                        title: Text(
                          "Log-out",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )

                      /*  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.power_settings_new,
                          color: Colors.white,
                        ),
                        SizedBox(width: 5),
                        Text(
                          "Log-out",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),*/
                      ),
                )
              ],
            ),
          ),
        ),
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.dehaze,
              color: color2(currentIndex),
            ),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          ),
          title: title(currentIndex),
          centerTitle: true,
          elevation: 0,
          backgroundColor: color(currentIndex),
          // actions: <Widget>[
          //   IconButton(
          //       icon: Icon(
          //         Icons.refresh,
          //         color: Colors.blue,
          //       ),
          //       onPressed: () {
          //         setState(() {

          //         });
          //       })
          // ],
        ),
        body: _pageChoser(currentIndex),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.timer),
              title: Text("Pending"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              title: Text("History"),
            ),
          ],
          currentIndex: currentIndex,
          unselectedItemColor: Colors.grey,
          selectedItemColor: Theme.of(context).primaryColor,
          onTap: (index) => setState(() {
            currentIndex = index;
          }),
        ),

        /* _pageChoser(currentIndex),
        bottomNavigationBar: BottomNavyBar(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          selectedIndex: currentIndex,
          showElevation: true,
          itemCornerRadius: 8,
          curve: Curves.easeInQuad,
          onItemSelected: (index) => setState(() {
            currentIndex = index;
          }),
          items: [
            BottomNavyBarItem(
              icon: Icon(Icons.timer),
              title: Text('Pending'),
              activeColor: Colors.blue,
              textAlign: TextAlign.center,
            ),
            BottomNavyBarItem(
              icon: Icon(Icons.list),
              title: Text('History'),
              activeColor: Colors.blue,
              textAlign: TextAlign.center,
            ),
          ],
        ),*/
      ),
    );
  }
}

drawerItems(IconData icon, String text, val) {
  return Container(
    child: Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 8, bottom: 8, top: 8),
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            color: Colors.lightBlueAccent,
          ),
          SizedBox(width: 15),
          Expanded(child: Text(text)),
          val,
        ],
      ),
    ),
  );
}

drawerNav(icon, text, fun) {
  return Container(
    child: GestureDetector(
      onTap: fun,
      child: Padding(
        padding:
            const EdgeInsets.only(left: 10.0, right: 4, bottom: 12, top: 8),
        child: Row(
          children: <Widget>[
            Icon(
              icon,
              color: Colors.lightBlueAccent,
            ),
            SizedBox(width: 15),
            Expanded(child: Text(text)),
          ],
        ),
      ),
    ),
  );
}
