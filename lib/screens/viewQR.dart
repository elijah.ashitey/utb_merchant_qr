import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/provider/User.dart';
import 'package:qr_approval/resources/constants.dart';
import 'package:qr_approval/widgets/networkSensitivity.dart';
import 'package:qr_flutter/qr_flutter.dart';

class VIewQR extends StatefulWidget {
  @override
  _VIewQRState createState() => _VIewQRState();
}

class _VIewQRState extends State<VIewQR> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserInfo>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text(
            "QR Image",
            // style: Theme.of(context).textTheme.title,
          ),
          centerTitle: true,
        ),
        body: NetworkSensitive(
          child:
              /* Center(
          child: QrImage(
            padding: EdgeInsets.all(8.0),
            data: "Elijah",
            version: QrVersions.auto,
            // embeddedImage: AssetImage('assets/images/utb_logo.png'),
            // embeddedImageStyle: QrEmbeddedImageStyle(
            //   size: Size(140, 80),
            // ),
            size: 320,
            // size: 0.4 * bodyHeight,
            errorStateBuilder: (cxt, err) {
              return Container(
                child: Center(
                  child: Text(
                    "Uh oh! Something went wrong...",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                ),
              );
            },
          ),
        ),*/

              Center(
                  child: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
            image: NetworkImage(
              "${Constants.apiPath}generate-user-qr/" +
                  user.getUser.data.userId,
              headers: Constants.header,
            ),
          )))
                  //          Image.network(
                  //   "${Constants.apiPath}generate-user-qr/" + user.getUser.data.userId,
                  // )

                  // ),
                  ),
        ));
  }
}
