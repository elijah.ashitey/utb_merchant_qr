// import 'package:bot_toast/bot_toast.dart';
// import 'package:flash/flash.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/models/pendingModel.dart';
// import 'package:qr_approval/provider/User.dart';
import 'package:qr_approval/provider/pending.dart';
import 'package:qr_approval/resources/amtformat.dart';
import 'package:qr_approval/resources/apiCalls.dart';
// import 'package:qr_approval/widgets/gradient.dart';
import 'package:qr_approval/widgets/networkSensitivity.dart';
import 'package:qr_approval/widgets/pedingList.dart';
import 'package:qr_approval/widgets/shimmerLoader.dart';
import 'dart:async';

// import 'package:toast/toast.dart';

// import 'package:flash/flash.dart';
// import 'package:flutter/foundation.dart';

class PendingPage extends StatefulWidget {
  final String id;
  PendingPage(this.id);
  @override
  _PendingPageState createState() => _PendingPageState();
}

class _PendingPageState extends State<PendingPage> {
  Future<PendingModel> data;

  bool moreData = true;
  Future<void> refresh() async {
    setState(() {
      data = pending(widget.id);
      _refresh = true;
    });
  }

  bool _refresh = true;

  // bool _visible = true;
  @override
  void initState() {
    super.initState(); //when this route starts, it will execute this code
    // final transData = Provider.of<Data>(context, listen: false);

    setState(() {
      _refresh = true;
    });

    Future.delayed(const Duration(seconds: 20), () {
      //asynchronous delay
      // if (transData.getMoreDate) {
      //   transData.setMoreData(false);
      //   print("Set widget off");
      // }
      if (this.mounted) {
        // print("Set widget off");
        //checks if widget is still active and not disposed
        setState(() {
          //tells the widget builder to rebuild again because ui has updated
          // _visible =
          //     false; //update the variable declare this under your class so its accessible for both your widget build and initState which is located under widget build{}
        });
      }
    });
    data = pending(widget.id);
  }
  // @override
  // void initState() {
  //   super.initState();

  //   data = pending(widget.id);
  // }
  Flushbar flush;
  // bool _wasButtonClicked;
  @override
  Widget build(BuildContext context) {
    // final user = Provider.of<UserInfo>(context);
    final transData = Provider.of<Data>(context);

    return Scaffold(
      floatingActionButton: GestureDetector(
        onTap: () => refresh(),
        child: CircleAvatar(
          child: Icon(
            Icons.refresh,
            color: Colors.white,
          ),
        ),
      ),
      body: NetworkSensitive(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bk1.jpg"), fit: BoxFit.cover),
          ),
          child: Container(
            color: Colors.white,
            // color: Colors.black26,
            child: Column(
              children: <Widget>[
                // _visible
                // ?
                _refresh
                    ? Consumer<Data>(builder: (context, _moreData, child) {
                        if (_moreData.getMoreDate) {
                          return Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.blue[50],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.info_outline,
                                      color: Colors.red,
                                      size: 30,
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Expanded(
                                      child: Text(
                                          "There are ${_moreData.getPending} pending transactions, approve or reject the current one to view the rest"),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        } else {
                          return Text("");
                        }
                        // if (_moreData.getPending != null) {
                        //   return Text(_moreData.getPending);
                        // } else {
                        //   return Text("");
                        // }
                      })
                    : Text(""),
                // : Text(""),
                Expanded(
                  child: Container(
                    child: FutureBuilder(
                        future: data,
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.connectionState ==
                                  ConnectionState.none ||
                              snapshot.connectionState ==
                                  ConnectionState.active ||
                              snapshot.connectionState ==
                                  ConnectionState.waiting) {
                            return Center(
                              child:
                                  // ShimmerImage(),
                                  HomeLoading(),
                            );
                          } else if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasError) {
                              final error = snapshot.error as PlatformException;
                              return Column(
                                children: [
                                  Image.asset(
                                    "assets/images/noData.png",
                                    width:
                                        MediaQuery.of(context).size.width * 0.8,
                                    height: MediaQuery.of(context).size.height *
                                        0.5,
                                  ),
                                  Center(child: Text(error.message)),
                                ],
                              );
                            } else if (snapshot.data == null) {
                              return Center(
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                    Text(
                                      "Error Loading Data",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    RaisedButton(
                                      color: Colors.blue,
                                      onPressed: () => refresh(),
                                      child: Text(
                                        "Retry",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    )
                                  ]));
                            } else if (snapshot.data != null) {
                              PendingModel res = snapshot.data;
                              // user.setPending(res);

                              if (res.responseCode == "000") {
                                transData.setPending(res.data);
                                if (res.data.length < 1) {
                                  return Center(
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                        Text(
                                          "No pending transaction",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        RaisedButton(
                                          color: Colors.blue,
                                          onPressed: () => refresh,
                                          child: Text(
                                            "Retry",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        )
                                      ]));
                                } else {
                                  transData.setPending(res.data);
                                  // if (res.data.length > 1) {
                                  // } else {}
                                  return RefreshIndicator(
                                    onRefresh: refresh,
                                    child: ListView.builder(
                                      // reverse: true,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        String currency = " ";
                                        if (res
                                                .data[
                                                    res.data.length - 1 - index]
                                                .currency !=
                                            null) {
                                          currency = res
                                              .data[res.data.length - 1 - index]
                                              .currency;
                                        } else {
                                          currency = "";
                                        }
                                        return Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: GestureDetector(
                                            onTap: () {
                                              transData.setData(res.data[
                                                  res.data.length - 1 - index]);
                                              Navigator.pushNamed(
                                                  context, ApproveRoute,
                                                  arguments: res.data[
                                                      res.data.length -
                                                          1 -
                                                          index]);
                                            },
                                            child: Card(
                                                elevation: 12,
                                                color: Colors.transparent,
                                                child: pendingShimmer(
                                                  context,
                                                  dateFormat(res
                                                      .data[res.data.length -
                                                          1 -
                                                          index]
                                                      .postingDate),
                                                  time(res
                                                      .data[res.data.length -
                                                          1 -
                                                          index]
                                                      .postingDate),
                                                  res
                                                      .data[res.data.length -
                                                          1 -
                                                          index]
                                                      .dbAccName,
                                                  res
                                                          .data[
                                                              res.data.length -
                                                                  1 -
                                                                  index]
                                                          .transId ??
                                                      " null",
                                                  currency +
                                                      " " +
                                                      formatAmounts(res
                                                          .data[
                                                              res.data.length -
                                                                  1 -
                                                                  index]
                                                          .amount),
                                                )),
                                          ),
                                        );
                                      },
                                      itemCount: 1,
                                      // itemCount: res.data.length,
                                    ),
                                  );
                                }
                              } else {
                                return Column(
                                  children: [
                                    Image.asset("assets/images/noData.png"),
                                    Center(
                                      child: res.message == null
                                          ? Text("Couldnt get Data")
                                          : Text(res.message),
                                    ),
                                  ],
                                );
                              }
                            } else {
                              return Center(
                                child: Text("error from no where"),
                              );
                            }
                          } else {
                            return Center(
                              child: Text("without snapshot error"),
                            );
                          }
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /*void _showTopFlash({FlashStyle style = FlashStyle.floating}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 2),
      persistent: false,
      builder: (_, controller) {
        return Flash(
          controller: controller,
          backgroundColor: Colors.white,
          brightness: Brightness.light,
          boxShadows: [BoxShadow(blurRadius: 4)],
          barrierBlur: 3.0,
          barrierColor: Colors.black38,
          barrierDismissible: true,
          style: style,
          position: FlashPosition.top,
          child: FlashBar(
            title: Text('Title'),
            message: Text('Hello world!'),
            showProgressIndicator: true,
            primaryAction: FlatButton(
              onPressed: () => controller.dismiss(),
              child: Text('DISMISS', style: TextStyle(color: Colors.amber)),
            ),
          ),
        );
      },
    );
  }*/
}

String dateFormat(DateTime date) {
  return DateFormat("d/M/y").format(date);
  // return DateFormat.d().add_yMMM().format(date);
}

String time(DateTime date) {
  return DateFormat("hh:mm a").format(date);
  // return DateFormat.d().add_yMMM().format(date);
}
