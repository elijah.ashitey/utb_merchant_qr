import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/provider/User.dart';
import 'package:qr_approval/resources/login.dart';
import 'package:qr_approval/widgets/Animation/fadeAnimation.dart';
import 'package:qr_approval/widgets/loading.dart';
// import 'package:qr_approval/widgets/gradient.dart';

class FirstTimeLogin extends StatefulWidget {
  final String pass;
  FirstTimeLogin(this.pass);
  @override
  _FirstTimeLoginState createState() => _FirstTimeLoginState();
}

class _FirstTimeLoginState extends State<FirstTimeLogin> {
  TextEditingController password = TextEditingController();
  TextEditingController username = TextEditingController();
  TextEditingController password1 = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isHidden = true;
  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  // @override
  // initState() {
  //   super.initState();
  //   final userData = Provider.of<UserInfo>(context, listen: false);
  //   String user = userData.getUser.data.userName;
  //   print(user);
  //   if (user == null || user.isEmpty || user.length == 0) {
  //     username = new TextEditingController();
  //   } else {
  //     username = new TextEditingController(text: user);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final def = Provider.of<Default>(context);
    final userData = Provider.of<UserInfo>(context);

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(
            "User Setup",
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
        ),
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bk7.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              // decoration: BoxDecoration(color: Color(0xff2323eb).withOpacity(0.7)),
              decoration: BoxDecoration(
                // color: Colors.blue.withOpacity(0.5),
                color: Colors.white.withOpacity(0.8),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.875,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FadeAnimation(
                          1.5,
                          Center(
                            child: Container(
                              margin: EdgeInsets.only(top: 50),
                              child: Column(
                                children: <Widget>[
                                  Text("Welcome,"),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 4.0),
                                        child: Icon(
                                          Icons.person,
                                          // color: Colors.blue,
                                          color: Color(0xff2323eb),

                                          // color: Color(0xff2323eb),
                                          size: 32.0,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Text(
                                        userData.getUser.data.fullName,
                                        style: TextStyle(
                                            // color: Colors.blue,
                                            color: Color(0xff2323eb),
                                            // color: Color(0xff2323eb),
                                            fontSize: 28,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02),
                      // ),
                      Container(
                        child: Padding(
                          padding: EdgeInsets.all(30.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FadeAnimation(
                                  2,
                                  Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.blueGrey,
                                              // Color.fromRGBO(143, 148, 251, .2),
                                              blurRadius: 20.0,
                                              offset: Offset(0, 10))
                                        ]),
                                    child: Form(
                                      key: _formKey,
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.all(8.0),
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color:
                                                            Colors.grey[100]))),
                                            child: TextFormField(
                                              // initialValue: "Elijah",
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return 'please enter username';
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "New Username",
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey[400])),
                                              controller: username,
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(8.0),
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color:
                                                            Colors.grey[100]))),
                                            child: TextFormField(
                                              obscureText: true,
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return 'please enter password';
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "New password ",
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey[400])),
                                              controller: password,
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(8.0),
                                            child: TextFormField(
                                              validator: (value) {
                                                if (value.isEmpty) {
                                                  return 'please enter password';
                                                }
                                                if (value.toString() !=
                                                    password.text) {
                                                  return 'password do not match';
                                                }
                                                return null;
                                              },
                                              controller: password1,
                                              obscureText: _isHidden,
                                              decoration: InputDecoration(
                                                  suffixIcon: IconButton(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                    onPressed:
                                                        _toggleVisibility,
                                                    icon: _isHidden
                                                        ? Icon(Icons
                                                            .visibility_off)
                                                        : Icon(
                                                            Icons.visibility),
                                                  ),
                                                  border: InputBorder.none,
                                                  hintText: "Confirm Password",
                                                  hintStyle: TextStyle(
                                                      color: Colors.grey[400])),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )),
                              SizedBox(
                                height: 30,
                              ),
                              FadeAnimation(
                                  2.5,
                                  def.getIsLoadind
                                      ? loading()
                                      : Container(
                                          height: 50,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            // gradient: more(),
                                            // color: Colors.blue,
                                            color: Color(0xff2323eb),
                                            // color: Color(0xffd3ef41),
                                          ),
                                          child: FlatButton(
                                            onPressed: () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                // auth(password.text, password.text,
                                                //     context);
                                                print(widget.pass);
                                                firstTime(
                                                    username.text,
                                                    widget.pass,
                                                    password.text,
                                                    context);
                                                print("correct");
                                              }
                                            },
                                            child: Center(
                                              child: Text(
                                                "Set-Up",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                        )),
                              // Padding(
                              //   padding: const EdgeInsets.all(15.0),
                              //   child: FadeAnimation(
                              //       3,
                              //       Text(
                              //         "Forgot Password?",
                              //         style: TextStyle(color: Colors.white),
                              //       )),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        )
        /* body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bk.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            color: Colors.blue.withOpacity(0.7),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.6,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(50.0),
                            ),
                          ),
                          filled: true,
                          hintStyle: TextStyle(color: Colors.grey[800]),
                          hintText: "password",
                          fillColor: Colors.white.withOpacity(0.8),
                          prefixIcon: Icon(Icons.person)),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),*/
        );
  }
}
