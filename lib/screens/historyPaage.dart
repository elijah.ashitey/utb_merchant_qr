import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:intl/intl.dart';
// import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/models/dailyHistory.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/provider/User.dart';

import 'package:qr_approval/resources/amtformat.dart';
import 'package:qr_approval/resources/apiCalls.dart';
import 'package:qr_approval/resources/closure.dart';
import 'package:qr_approval/widgets/dialogs.dart';
// import 'package:qr_approval/screens/pending.dart';
import 'package:qr_approval/widgets/funtionWidget.dart';
import 'package:qr_approval/widgets/gradient.dart';
import 'package:qr_approval/widgets/historyLoading.dart';
import 'package:qr_approval/widgets/networkSensitivity.dart';
import 'package:qr_approval/widgets/pedingList.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
// import 'package:qr_approval/widgets/shimmerLoader.dart';

class History extends StatefulWidget {
  final String id;
  History(this.id);
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  Future<DatehistoryModel> data;
  ScrollController _hideButtonController;
  int total;
  // DateTime _date = DateTime.now();

  // Future<Null> selectDate(BuildContext context) async {
  //   final DateTime picked = await showDatePicker(
  //     context: context,
  //     initialDate: _date,
  //     firstDate: DateTime(1930),
  //     lastDate: DateTime(2100),
  //   );
  //   if (picked != null && picked != _date) {
  //     setState(() {
  //       _date = picked;
  //       print(_date.toString());
  //     });
  //   }
  // }

  // String dateFormat(DateTime date) {
  //   return DateFormat("dd-MMM-yyyy").format(date);
  //   // return DateFormat.yMMMd().format(_date);
  // }

  String date;

  Future<void> refresh() async {
    setState(() {
      data = historybyDate(widget.id, date, context);
    });
  }

  var _isVisible;
  @override
  void initState() {
    super.initState();
    date = DateFormat("dd-MM-yyyy").format(_date);
    // date = "04-12-2019";
    data = historybyDate(widget.id, date, context);
    _isVisible = true;
    _hideButtonController = new ScrollController();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (_isVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds 
             */
          print("**** $_isVisible up"); //Move IO away from setState
          setState(() {
            _isVisible = false;
          });
        }
      } else {
        if (_hideButtonController.position.userScrollDirection ==
            ScrollDirection.forward) {
          if (_isVisible == false) {
            /* only set when the previous state is false
               * Less widget rebuilds 
               */
            print("**** $_isVisible down"); //Move IO away from setState
            setState(() {
              _isVisible = true;
            });
          }
        }
      }
    });
  }

  DateTime _date = DateTime.now();
  String today = DateFormat("dd-MM-yyyy").format(DateTime.now());

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1930),
      lastDate: DateTime(2100),
    );
    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
        print(_date.toString());
      });
    }
  }

  String newdateFormat(DateTime date) {
    return DateFormat("dd-MMM-yyyy").format(date);
    // return DateFormat.yMMMd().format(_date);
  }

  TextEditingController password = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // final transData = Provider.of<HistoryData>(context);
    final user = Provider.of<UserInfo>(context);
    final def = Provider.of<Default>(context);
    var alertStyle = AlertStyle(
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isOverlayTapDismiss: true,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      animationDuration: Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.red,
      ),
    );
    return Scaffold(
      // floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      floatingActionButton: today == date && def.getClosureOff
          ? Visibility(
              visible: _isVisible,
              child: Container(
                // color: Colors.blue,
                child: def.getIsLoadind
                    ? CircularProgressIndicator()
                    : FlatButton(
                        color: Colors.blue,
                        onPressed: () {
                          conf(
                              context, "Do you want to close this transaction?",
                              () {
                            // Navigator.pop(context);
                            Alert(
                                style: alertStyle,
                                context: context,
                                title: "Enter Password",
                                content: Form(
                                  key: _formKey,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      TextFormField(
                                        obscureText: true,
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'please enter password';
                                          }
                                          if (value.length < 8) {
                                            return 'password should be more than 8 character';
                                          }
                                          return null;
                                        },
                                        decoration: InputDecoration(
                                          icon: Icon(Icons.lock),
                                          // border: InputBorder.none,
                                          labelText: 'Password',
                                          hintStyle: TextStyle(
                                              color: Colors.grey[400]),
                                        ),
                                        controller: password,
                                      ),
                                      /*   TextField(
                                        obscureText: true,
                                        decoration: InputDecoration(
                                          icon: Icon(Icons.lock),
                                          labelText: 'Password',
                                        ),
                                        controller: password,
                                      ),*/
                                    ],
                                  ),
                                ),
                                buttons: [
                                  DialogButton(
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        Navigator.pop(context);
                                        closureLogic(
                                            password.text, context, refresh);
                                        password.clear();
                                      }
                                    },
                                    child: Text(
                                      "LOGIN",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20),
                                    ),
                                  )
                                ]).show();
                          });
                          // () => closureLogic(user.getUser.data.userId, context));
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(
                              Icons.close,
                              color: Colors.white,
                            ),
                            Text(
                              'Closure',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                              ),
                            ),
                          ],
                        ),
                        // textColor: Colors.red,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Colors.white,
                                width: 1,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(6)),
                      ),
              ),
            )
          : Container(),
      body: Column(
        children: <Widget>[
          NetworkSensitive(
            child: Expanded(
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/bk1.jpg"),
                      fit: BoxFit.cover),
                ),
                child: Container(
                  color: Colors.black26,
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          // borderRadius: BorderRadius.only(
                          //   bottomLeft: Radius.circular(15),
                          //   bottomRight: Radius.circular(15),
                          // ),
                          color: Colors.blue[50],
                        ),
                        // height: MediaQuery.of(context).size.height * 0.07,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  selectDate(context);
                                },
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Select Date",
                                      style: TextStyle(
                                          color: Colors.blue, fontSize: 18),
                                    ),
                                    Text(newdateFormat(_date)),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  selectDate(context);
                                },
                                child: Icon(
                                  Icons.date_range,
                                  color: Colors.grey,
                                  // color: _iconColor,
                                ),
                              ),
                              RaisedButton(
                                color: Colors.blue,
                                onPressed: () {
                                  setState(() {
                                    date =
                                        DateFormat("dd-MM-yyyy").format(_date);
                                    data =
                                        historybyDate(widget.id, date, context);
                                  });
                                  print(DateFormat("dd-MM-yyyy").format(_date));
                                },
                                child: Text('Search',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18.0)),
                                // textColor: Colors.red,
                                /* shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Colors.blue,
                                width: 1,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(6)),*/
                              ),
                              // RaisedButton(
                              //   color: Colors.blue,
                              //   onPressed: () {
                              //     setState(() {
                              //       date = DateFormat("dd-MM-yyyy").format(_date);
                              //       data = historybyDate(widget.id, date);
                              //     });
                              //     print(DateFormat("dd-MM-yyyy").format(_date));
                              //   },
                              //   child: Text('Search',
                              //       style: TextStyle(
                              //           color: Colors.white, fontSize: 18.0)),
                              //   // textColor: Colors.red,
                              //   /* shape: RoundedRectangleBorder(
                              //       side: BorderSide(
                              //           color: Colors.blue,
                              //           width: 1,
                              //           style: BorderStyle.solid),
                              //       borderRadius: BorderRadius.circular(6)),*/
                              // ),
                            ]),
                      ),
                      Expanded(
                        child: Container(
                          child: FutureBuilder(
                              future: data,
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.none ||
                                    snapshot.connectionState ==
                                        ConnectionState.active ||
                                    snapshot.connectionState ==
                                        ConnectionState.waiting) {
                                  return Center(
                                    child: HistoryLoading(),
                                  );
                                } else if (snapshot.connectionState ==
                                    ConnectionState.done) {
                                  if (snapshot.hasError) {
                                    // final error = snapshot.error as PlatformException;
                                    return Center(
                                        child: Text("Error:" +
                                            snapshot.error.toString()));
                                  } else if (snapshot.data == null) {
                                    return Center(
                                        child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                          Text(
                                            "Error Loading Data",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          RaisedButton(
                                            color: Colors.blue,
                                            onPressed: () => refresh(),
                                            child: Text(
                                              "Retry",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          )
                                        ]));
                                  } else if (snapshot.data != null) {
                                    DatehistoryModel res = snapshot.data;
                                    // user.setPending(res);

                                    if (res.responseCode == "200") {
                                      // transData.setRejected(res.data);
                                      total = res.data.transactions.length;

                                      if (res.data.transactions.length < 1) {
                                        return Container(
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              Container(
                                                color: Colors.white,
                                                child: Card(
                                                  elevation: 0,
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: <Widget>[
                                                      Text(
                                                        "Amount made",
                                                        style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          GradientText(
                                                            def.getCurrency,
                                                            gradient: basic(),
                                                            style: TextStyle(
                                                                fontSize: 15),
                                                          ),
                                                          SizedBox(
                                                            width: 3,
                                                          ),
                                                          Text(
                                                            formatAmounts(res
                                                                .data
                                                                .totalAmount
                                                                .toString()),
                                                            // "999,000,000,000,000,000.00",
                                                            // gradient: basic(),
                                                            style: TextStyle(
                                                              fontSize: 32,
                                                              color:
                                                                  Colors.blue,
                                                              // fontWeight:
                                                              //     FontWeight.bold,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      // SizedBox(
                                                      //   height: 5,
                                                      // ),
                                                      Container(
                                                          child: Text(
                                                              res.message)),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      } else {
                                        // trans = res.data.transactions;
                                        return Column(
                                          children: <Widget>[
                                            Container(
                                              color: Colors.white,
                                              child: Card(
                                                elevation: 0,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    Text(
                                                      "Amount made",
                                                      style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        GradientText(
                                                          def.getCurrency,
                                                          gradient: basic(),
                                                          style: TextStyle(
                                                              fontSize: 15),
                                                        ),
                                                        SizedBox(
                                                          width: 3,
                                                        ),
                                                        Text(
                                                          formatAmounts(res
                                                              .data.totalAmount
                                                              .toString()),
                                                          // "999,000,000,000,000,000.00",
                                                          // gradient: basic(),
                                                          style: TextStyle(
                                                            fontSize: 32,
                                                            color: Colors.blue,
                                                            // fontWeight:
                                                            //     FontWeight.bold,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Container(
                                                color: Colors.white,
                                                child: RefreshIndicator(
                                                  onRefresh: refresh,
                                                  child: ListView.builder(
                                                    controller:
                                                        _hideButtonController,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return historyWidget(
                                                          context,
                                                          def.getCurrency +
                                                              " " +
                                                              formatAmounts(res
                                                                  .data
                                                                  .transactions[
                                                                      index]
                                                                  .amount),
                                                          accNum(res
                                                              .data
                                                              .transactions[
                                                                  index]
                                                              .crAccNum),
                                                          res
                                                              .data
                                                              .transactions[
                                                                  index]
                                                              .transId,
                                                          user.getUser.data
                                                              .fullName,
                                                          res
                                                              .data
                                                              .transactions[
                                                                  index]
                                                              .postingDate,
                                                          res
                                                              .data
                                                              .transactions[
                                                                  index]
                                                              .statusFlag);
                                                    },
                                                    itemCount: res.data
                                                        .transactions.length,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      }
                                    } else {
                                      return Center(
                                        child: res.message == null
                                            ? Text("Couldnt get Data")
                                            : Text(res.message),
                                      );
                                    }
                                  } else {
                                    return Center(
                                      child: Text("error from no where"),
                                    );
                                  }
                                } else {
                                  return Center(
                                    child: Text("without snapshot error"),
                                  );
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          /*Align(
            alignment: Alignment.bottomRight,
            child: Container(
              // height: MediaQuery.of(context).size.height * 0.1,

              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  // mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    /*   GestureDetector(
                      onTap: () => refresh(),
                      child: CircleAvatar(
                        child: Icon(
                          Icons.refresh,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.01,
                    ),*/
                    GestureDetector(
                      // onTap: () => refresh(),
                      child: Container(
                        // color: Colors.blue,
                        child: def.getIsLoadind
                            ? CircularProgressIndicator()
                            : FlatButton(
                                color: Colors.blue,
                                onPressed: () {
                                  conf(
                                      context,
                                      "Do you want to close this transaction?",
                                      () => closureLogic(
                                          user.getUser.data.userId, context));
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Icon(
                                      Icons.close,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      'Closure',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                      ),
                                    ),
                                  ],
                                ),
                                // textColor: Colors.red,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: Colors.white,
                                        width: 1,
                                        style: BorderStyle.solid),
                                    borderRadius: BorderRadius.circular(6)),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )*/
        ],
      ),
    );
  }
}

class Detail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            Text("Detail"),
          ],
        ),
      ),
    );
  }
}
