// To parse this JSON data, do
//
//     final datehistoryModel = datehistoryModelFromJson(jsonString);

import 'dart:convert';

DatehistoryModel datehistoryModelFromJson(String str) =>
    DatehistoryModel.fromJson(json.decode(str));

String datehistoryModelToJson(DatehistoryModel data) =>
    json.encode(data.toJson());

class DatehistoryModel {
  DatehistoryModel({
    this.responseCode,
    this.message,
    this.data,
  });

  String responseCode;
  String message;
  Data data;

  factory DatehistoryModel.fromJson(Map<String, dynamic> json) =>
      DatehistoryModel(
        responseCode: json["responseCode"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.totalAmount,
    this.transactions,
  });

  double totalAmount;
  List<Transaction> transactions;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        totalAmount: double.parse(json["total amount"].toString()),
        transactions: List<Transaction>.from(
            json["transactions"].map((x) => Transaction.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total amount": totalAmount,
        "transactions": List<dynamic>.from(transactions.map((x) => x.toJson())),
      };
}

class Transaction {
  Transaction({
    this.dbAccName,
    this.dbAccNum,
    this.crAccName,
    this.crAccNum,
    this.postingDate,
    this.amount,
    this.narration,
    this.approvedBy,
    this.secPin,
    this.deviceIp,
    this.tokenId,
    this.transId,
    this.statusFlag,
    this.rejectReason,
    this.currency,
    this.batchNo,
    this.fullName,
    this.customerNo,
  });

  String dbAccName;
  String dbAccNum;
  String crAccName;
  String crAccNum;
  DateTime postingDate;
  String amount;
  String narration;
  String approvedBy;
  String secPin;
  String deviceIp;
  String tokenId;
  String transId;
  String statusFlag;
  dynamic rejectReason;
  String currency;
  String batchNo;
  String fullName;
  String customerNo;

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
        dbAccName: json["db_acc_name"],
        dbAccNum: json["db_acc_num"],
        crAccName: json["cr_acc_name"],
        crAccNum: json["cr_acc_num"],
        postingDate: DateTime.parse(json["posting_date"]),
        amount: json["amount"],
        narration: json["narration"],
        approvedBy: json["approved_by"],
        secPin: json["sec_pin"],
        deviceIp: json["device_ip"],
        tokenId: json["token_id"],
        transId: json["trans_id"],
        statusFlag: json["status_flag"],
        rejectReason: json["reject_reason"],
        currency: json["currency"],
        batchNo: json["batch_no"],
        fullName: json["full_name"],
        customerNo: json["customer_no"],
      );

  Map<String, dynamic> toJson() => {
        "db_acc_name": dbAccName,
        "db_acc_num": dbAccNum,
        "cr_acc_name": crAccName,
        "cr_acc_num": crAccNum,
        "posting_date": postingDate.toIso8601String(),
        "amount": amount,
        "narration": narration,
        "approved_by": approvedBy,
        "sec_pin": secPin,
        "device_ip": deviceIp,
        "token_id": tokenId,
        "trans_id": transId,
        "status_flag": statusFlag,
        "reject_reason": rejectReason,
        "currency": currency,
        "batch_no": batchNo,
        "full_name": fullName,
        "customer_no": customerNo,
      };
}
