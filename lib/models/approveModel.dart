// To parse this JSON data, do
//
//     final approveModel = approveModelFromJson(jsonString);

import 'dart:convert';

ApproveModel approveModelFromJson(String str) =>
    ApproveModel.fromJson(json.decode(str));

String approveModelToJson(ApproveModel data) => json.encode(data.toJson());

class ApproveModel {
  String responseCode;
  String message;
  dynamic data;

  ApproveModel({
    this.responseCode,
    this.message,
    this.data,
  });

  factory ApproveModel.fromJson(Map<String, dynamic> json) => ApproveModel(
        responseCode: json["responseCode"],
        message: json["message"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "message": message,
        "data": data,
      };
}
