// To parse this JSON data, do
//
//     final pendingModel = pendingModelFromJson(jsonString);

import 'dart:convert';

PendingModel pendingModelFromJson(String str) =>
    PendingModel.fromJson(json.decode(str));

String pendingModelToJson(PendingModel data) => json.encode(data.toJson());

class PendingModel {
  String responseCode;
  String message;
  List<Datum> data;

  PendingModel({
    this.responseCode,
    this.message,
    this.data,
  });

  factory PendingModel.fromJson(Map<String, dynamic> json) => PendingModel(
        responseCode: json["responseCode"],
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  String dbAccName;
  String dbAccNum;
  String crAccName;
  String crAccNum;
  DateTime postingDate;
  String amount;
  String narration;
  String approvedBy;
  String secPin;
  String deviceIp;
  String tokenId;
  String transId;
  dynamic statusFlag;
  dynamic rejectReason;
  String currency;

  Datum({
    this.dbAccName,
    this.dbAccNum,
    this.crAccName,
    this.crAccNum,
    this.postingDate,
    this.amount,
    this.narration,
    this.approvedBy,
    this.secPin,
    this.deviceIp,
    this.tokenId,
    this.transId,
    this.statusFlag,
    this.rejectReason,
    this.currency,
  });

  factory Datum.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }
    return Datum(
      dbAccName: json["db_acc_name"],
      dbAccNum: json["db_acc_num"],
      crAccName: json["cr_acc_name"],
      crAccNum: json["cr_acc_num"],
      postingDate: DateTime.parse(json["posting_date"]),
      amount: json["amount"],
      narration: json["narration"],
      approvedBy: json["approved_by"],
      secPin: json["sec_pin"],
      deviceIp: json["device_ip"],
      tokenId: json["token_id"],
      transId: json["trans_id"],
      statusFlag: json["status_flag"],
      rejectReason: json["reject_reason"],
      currency: json["currency"],
    );
  }

  Map<String, dynamic> toJson() => {
        "db_acc_name": dbAccName,
        "db_acc_num": dbAccNum,
        "cr_acc_name": crAccName,
        "cr_acc_num": crAccNum,
        "posting_date": postingDate.toIso8601String(),
        "amount": amount,
        "narration": narration,
        "approved_by": approvedBy,
        "sec_pin": secPin,
        "device_ip": deviceIp,
        "token_id": tokenId,
        "trans_id": transId,
        "status_flag": statusFlag,
        "reject_reason": rejectReason,
        "currency": currency,
      };
}
