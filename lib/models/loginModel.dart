// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    this.responseCode,
    this.message,
    this.data,
  });

  String responseCode;
  String message;
  Data data;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        responseCode: json["responseCode"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.userId,
    this.userName,
    this.fullName,
    this.email,
    this.phone,
    this.userImage,
    this.status,
    this.specialAccess,
    this.xApiKey,
    this.customerNo,
    this.accountNo,
    this.fLogin,
    this.accountDesc,
  });

  String userId;
  String userName;
  String fullName;
  String email;
  String phone;
  String userImage;
  String status;
  String specialAccess;
  dynamic xApiKey;
  String customerNo;
  String accountNo;
  String fLogin;
  String accountDesc;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        userId: json["user_id"],
        userName: json["user_name"],
        fullName: json["full_name"],
        email: json["email"],
        phone: json["phone"],
        userImage: json["user_image"],
        status: json["status"],
        specialAccess: json["special_access"],
        xApiKey: json["x-api-key"],
        customerNo: json["customer_no"],
        accountNo: json["account_no"],
        fLogin: json["f_login"],
        accountDesc: json["account_desc"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "user_name": userName,
        "full_name": fullName,
        "email": email,
        "phone": phone,
        "user_image": userImage,
        "status": status,
        "special_access": specialAccess,
        "x-api-key": xApiKey,
        "customer_no": customerNo,
        "account_no": accountNo,
        "f_login": fLogin,
        "account_desc": accountDesc,
      };
}
