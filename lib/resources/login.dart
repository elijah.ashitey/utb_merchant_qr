import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/provider/User.dart';
import 'package:qr_approval/widgets/dialogs.dart';
// import 'package:qr_approval/widgets/load.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'apiCalls.dart';
import 'internetCheck.dart';

auth(username, password, context) async {
  final userData = Provider.of<UserInfo>(context, listen: false);
  final def = Provider.of<Default>(context, listen: false);
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();

  try {
    def.setLoading(true);
    String savedpin = sharedPrefs.getString("pin") ?? "";
    print("pin :" + savedpin);
    def.setPin(savedpin);
    print(username);
    print(password);
    final checkinternet = await internetCheck();
    if (checkinternet) {
      final res = await login(username, password);
      userData.setUser(res);
      // def.setTimeOut(false);
      if (res.data.fLogin == "YES") {
        Navigator.pushNamed(context, FirstTimeRoute, arguments: password);
      } else {
        if (savedpin.length == 4) {
          Navigator.pushNamed(context, HomePageRoute);
        } else {
          Navigator.pushNamed(context, PinPageRoute);
        }
      }
    } else {
      info(context, "Please check your internet connection");
    }
  } on PlatformException catch (e) {
    print(e.toString());
    err(context, e.message.toString());
  } finally {
    def.setLoading(false);
  }
}

firstTime(username, old, password, context) async {
  final userData = Provider.of<UserInfo>(context, listen: false);
  final def = Provider.of<Default>(context, listen: false);
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();

  try {
    def.setLoading(true);
    String savedpin = sharedPrefs.getString("pin") ?? "";
    print("pin :" + savedpin);
    def.setPin(savedpin);
    print(username);
    print(password);
    print("--------------------");
    print(userData.getUser.data.userId);
    print(password);
    print(old);
    print(password);
    final checkinternet = await internetCheck();
    if (checkinternet) {
      final res = await userSetup(
          userData.getUser.data.userId, username, old, password);

      // def.setTimeOut(false);

      if (savedpin.length == 4) {
        Navigator.pushNamed(context, HomePageRoute);
      } else {
        Navigator.pushNamed(context, PinPageRoute);
      }
    } else {
      info(context, "Please check your internet connection");
    }
  } on PlatformException catch (e) {
    print(e.toString());
    err(context, e.message.toString());
  } finally {
    def.setLoading(false);
  }
}
