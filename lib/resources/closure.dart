import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/provider/User.dart';

import 'package:qr_approval/widgets/dialogs.dart';

import 'apiCalls.dart';
import 'internetCheck.dart';

closureLogic(password, context, Function fun) async {
  final def = Provider.of<Default>(context, listen: false);
  final user = Provider.of<UserInfo>(context, listen: false);

  try {
    def.setLoading(true);
    final checkinternet = await internetCheck();

    if (checkinternet) {
      final res = await closure(
        user.getUser.data.userId,
        user.getUser.data.userName,
        password,
      );
      def.setClosureOff(false);

      success(context, res, fun);
    }
  } on PlatformException catch (e) {
    print(e.toString());
    err(context, e.message.toString());
  } finally {
    def.setLoading(false);
  }
}
