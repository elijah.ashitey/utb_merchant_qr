import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/widgets/dialogs.dart';
// import 'package:provider/provider.dart';
// import 'package:qr_approval/Routes/routeConstants.dart';
// import 'package:qr_approval/provider/User.dart';

import 'apiCalls.dart';
import 'internetCheck.dart';

approveLogic(transID, userID, context) async {
  // final userData = Provider.of<UserInfo>(context, listen: false);
  final def = Provider.of<Default>(context, listen: false);

  try {
    def.setLoading(true);
    print(transID);
    print(userID);
    final checkinternet = await internetCheck();
    if (checkinternet) {
      final res = await approve(transID, userID);
      // userData.setUser(res);
      Navigator.pushNamed(context, SuccessRoute, arguments: res.message);
      print(res.message);
    } else {
      print("No internet");
    }
  } on PlatformException catch (e) {
    print(e.message.toString());
    err(context, e.message.toString());
  } finally {
    def.setLoading(false);
  }
}

rejectLogic(transID, userID, comment, context) async {
  // final userData = Provider.of<UserInfo>(context, listen: false);
  final def = Provider.of<Default>(context, listen: false);

  try {
    def.setLoading(true);

    print(transID);
    print(userID);
    final checkinternet = await internetCheck();
    if (checkinternet) {
      final res = await reject(transID, userID, comment);
      // userData.setUser(res);
      Navigator.pushNamed(context, SuccessRoute, arguments: res.message);

      print(res.message);
    } else {
      print("No internet");
    }
  } on PlatformException catch (e) {
    print(e.message.toString());
    err(context, e.message.toString());
  } finally {
    def.setLoading(false);
  }
}
