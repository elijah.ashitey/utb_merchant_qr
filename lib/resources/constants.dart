class Constants {
  // static final String url = "http://41.223.135.173:80/usg_qr/public/";
  static final String url =
      //  "http://192.168.1.215:86/usg_qr/public/";
      "http://merchant.utb.sl/";
  static final String apiPath = url + "api/v1/";
  static final String image = url + "uploads/";
  static final dynamic header = {
    // "Content-Type": "application/json",
    "x-api-key": "QR-PORTAL-API",
  };
}
