import 'package:flutter/material.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/widgets/dialogs.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:provider/provider.dart';
import 'sharedPrefrences.dart';

pinConfig(String pin, BuildContext context) async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  final def = Provider.of<Default>(context, listen: false);
  try {
    def.setLoading(true);

    if (pin != null) {
      save("pin", pin);
      String savedpin = sharedPrefs.getString("pin");
      print(savedpin);
      def.setPin(savedpin);
      success(context, "Successful Pin setup", () {
        Navigator.pushNamed(context, HomePageRoute);
      });
    }
  } catch (e) {
    err(context, "An error occured");
  } finally {
    def.setLoading(false);
  }
}
