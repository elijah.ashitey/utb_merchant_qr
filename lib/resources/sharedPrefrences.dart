import 'package:shared_preferences/shared_preferences.dart';

save(String key, dynamic value) async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  if (value is bool) {
    sharedPrefs.setBool(key, value);
  } else if (value is String) {
    sharedPrefs.setString(key, value);
  } else if (value is int) {
    sharedPrefs.setInt(key, value);
  } else if (value is double) {
    sharedPrefs.setDouble(key, value);
  } else if (value is List<String>) {
    sharedPrefs.setStringList(key, value);
  }
}
/*
Future<bool> isDataSaved() async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  if (sharedPrefs.getString("username") != null &&
      sharedPrefs.getString("password") != null) {
    return true;
  } else {
    return false;
  }
}

getusername() async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  String username = sharedPrefs.getString("username");
  return username;
}

getpassword() async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  String username = sharedPrefs.getString("password");
  return username;
}
*/
