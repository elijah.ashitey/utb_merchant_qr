import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';

Future<bool> authenticate() async {
  final _auth = LocalAuthentication();

  bool isAuthenticated;
  try {
    isAuthenticated = await _auth.authenticateWithBiometrics(
      localizedReason: 'authenticate to access',
      useErrorDialogs: true,
      stickyAuth: true,
    );
    // print(isAuthenticated);
  } on PlatformException catch (e) {
    print(e);
  }
  return isAuthenticated;
}
