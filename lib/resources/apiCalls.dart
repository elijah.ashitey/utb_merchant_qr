import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:qr_approval/models/approveModel.dart';
import 'package:qr_approval/models/dailyHistory.dart';
import 'package:qr_approval/models/historyModel.dart';
import 'package:qr_approval/models/loginModel.dart';
import 'package:qr_approval/models/pendingModel.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/provider/hitory.dart';
import 'constants.dart';
// import 'package:flutter/services.dart';
import 'dart:convert';

Future<LoginModel> login(username, password) async {
  final url = "${Constants.apiPath}login";
  final body = {
    "username": username,
    "password": password,
  };
  final response = await http.post(
    url,
    headers: Constants.header,
    body: body,
  );
  print(response.body);
  if (response.statusCode == 200) {
    Map<String, dynamic> result = json.decode(response.body);

    if (result["responseCode"] == "000") {
      LoginModel responds = LoginModel.fromJson(json.decode(response.body));

      return responds;
    } else {
      throw PlatformException(
        code: result["responseCode"].toString(),
        message: result["message"],
      );
    }
    // return responds;
  } else {
    // return null;
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<String> userSetup(id, username, oldpassword, password) async {
  final url = "${Constants.apiPath}change-cashier-credentials";
  // final body = {
  //   "user_id": id,
  //   "username": username,
  //   "old_password": oldpassword,
  //   "new_password": password,
  //   "confirm_new_password": password
  // };
  print(id);
  print(username);
  print(oldpassword);
  print(password);
  final body = {
    "user_id": id,
    "username": username,
    "old_password": oldpassword,
    "new_password": password,
    "confirm_new_password": password
  };
  final response = await http.post(
    url,
    headers: Constants.header,
    body: body,
  );
  print(response.body);
  if (response.statusCode == 200) {
    Map<String, dynamic> result = json.decode(response.body);

    if (result["responseCode"] == "000") {
      return result["message"];
    } else {
      throw PlatformException(
        code: result["responseCode"].toString(),
        message: result["message"],
      );
    }
    // return responds;
  } else {
    // return null;
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<String> changePasswordAPi(id, old, newpassword) async {
  final url = "${Constants.apiPath}change-password";
  final body = {
    "user_id": id,
    "old_password": old,
    "new_password": newpassword,
    "confirm_new_password": newpassword
  };
  final response = await http.post(
    url,
    headers: Constants.header,
    body: body,
  );
  print(response.body);
  if (response.statusCode == 200) {
    Map<String, dynamic> result = json.decode(response.body);
    // LoginModel responds = LoginModel.fromJson(json.decode(response.body));
    if (result["responseCode"] == "000") {
      return result["message"];
    } else {
      throw PlatformException(
        code: result["responseCode"].toString(),
        message: result["message"],
      );
    }
    // return responds;
  } else {
    // return null;
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<PendingModel> pending(id) async {
  final url = "${Constants.apiPath}pending-payments/$id";

  final response = await http.get(
    url,
    headers: Constants.header,
  );
  print(response.body);
  if (response.statusCode == 200) {
    Map<String, dynamic> result = json.decode(response.body);
    if (result["responseCode"] == "000") {
      PendingModel responds = PendingModel.fromJson(json.decode(response.body));
      return responds;
    } else {
      throw PlatformException(
          code: result["responseCode"], message: result["message"]);
    }
  } else {
    return null;
  }
}

Future<HistoryModel> history(String id, BuildContext context) async {
  final history = Provider.of<HistoryData>(context, listen: false);
  final url = "${Constants.apiPath}get-transaction/$id";
  final response = await http.get(
    url,
    headers: Constants.header,
  );
  print(id);
  print(response.body);
  if (response.statusCode == 200) {
    Map<String, dynamic> result = json.decode(response.body);
    if (result["responseCode"] == "000") {
      HistoryModel responds = HistoryModel.fromJson(json.decode(response.body));
      history.setRejected(responds.data);

      print(history.getRjected);
      print("dataa" + history.getGetApproved);
      return responds;
    } else {
      throw PlatformException(
          code: result["responseCode"], message: result["message"]);
    }
  } else {
    return null;
    // throw PlatformException(code: "Error", message: "Error Getting Data");
  }
}

Future<String> closure(id, username, password) async {
  final url = "${Constants.apiPath}submit-closure";
  final body = {"user_id": id, "username": username, "password": password};
  final response = await http.post(
    url,
    headers: Constants.header,
    body: body,
  );
  print(response.body);

  if (response.statusCode == 200) {
    Map<String, dynamic> result = json.decode(response.body);

    if (result["responseCode"] == "000") {
      // HistoryModel responds = HistoryModel.fromJson(json.decode(response.body));

      return result["message"];
    } else {
      throw PlatformException(
        code: result["responseCode"],
        message: result["message"],
      );
    }
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<DatehistoryModel> historybyDate(id, date, BuildContext context) async {
  final def = Provider.of<Default>(context, listen: false);

  final url = "${Constants.apiPath}get-transaction-date/$id/$date";

  final response = await http.get(
    url,
    headers: Constants.header,
  );
  print(response.body);
  if (response.statusCode == 200) {
    DatehistoryModel responds =
        DatehistoryModel.fromJson(json.decode(response.body));
    print("printing the length from API:" +
        responds.data.transactions.length.toString());
    for (var item in responds.data.transactions) {
      if (item.statusFlag == "Y") {
        def.setClosureOff(true);
      }
    }

    return responds;

    /*  Map<String, dynamic> result = json.decode(response.body);
    if (result["responseCode"] == "200") {
   
    } else {
      throw PlatformException(
          code: result["responseCode"], message: result["message"]);
    }*/
  } else {
    return null;
    // throw PlatformException(
    //     code: response.statusCode.toString(),
    //     message: "Error connecting to server");
  }
}

Future<ApproveModel> approve(transID, userID) async {
  // final url = "http://merchant.utb.sl/api/v1/approve-payment";
  final url = "${Constants.apiPath}approve-payment";
  final body = {
    "trans_id": transID,
    "user_id": userID,
  };
  final response = await http.post(
    url,
    headers: Constants.header,
    body: body,
  );
  print(response.body);
  if (response.statusCode == 200) {
    ApproveModel responds = ApproveModel.fromJson(json.decode(response.body));
    if (responds.responseCode == "000") {
      return responds;
    } else {
      throw PlatformException(
        code: "API Error",
        message: responds.message,
      );
    }
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<ApproveModel> reject(transID, userID, comment) async {
  final url = "${Constants.apiPath}reject-payment";
  final body = {
    "trans_id": transID,
    "user_id": userID,
    "comment": comment,
  };
  final response = await http.post(
    url,
    headers: Constants.header,
    body: body,
  );
  print(response.body);
  if (response.statusCode == 200) {
    ApproveModel responds = ApproveModel.fromJson(json.decode(response.body));
    if (responds.responseCode == "000") {
      return responds;
    } else {
      throw PlatformException(
        code: "API Error",
        message: responds.message,
      );
    }
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}
