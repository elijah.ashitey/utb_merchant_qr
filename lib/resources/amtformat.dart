import 'package:intl/intl.dart';

String formatAmounts(dynamic amt) {
  String formattedAmt;
  final formatter = new NumberFormat("#,##0.00", "en_US");
  // final formatter = new NumberFormat.simpleCurrency();

  if (amt.isEmpty || amt == '0') {
    amt = "0.00";
    formattedAmt = amt;
  } else {
    double amtDouble = double.parse(amt);
    formattedAmt = formatter.format(amtDouble);
  }
  return formattedAmt;
}
