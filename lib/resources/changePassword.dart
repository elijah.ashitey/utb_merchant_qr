import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:qr_approval/Routes/routeConstants.dart';
import 'package:qr_approval/provider/Defaults.dart';
import 'package:qr_approval/provider/User.dart';
import 'package:qr_approval/widgets/dialogs.dart';
// import 'package:qr_approval/widgets/load.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'apiCalls.dart';
import 'internetCheck.dart';

changePassword(old, newpassword, context) async {
  final userData = Provider.of<UserInfo>(context, listen: false);
  final def = Provider.of<Default>(context, listen: false);

  try {
    def.setLoading(true);

    final checkinternet = await internetCheck();
    if (checkinternet) {
      final res = await changePasswordAPi(
          userData.getUser.data.userId, old, newpassword);
      success(context, res, () {
        Navigator.pushNamed(context, HomePageRoute);
      });
    }
  } on PlatformException catch (e) {
    print(e.toString());
    err(context, e.message.toString());
  } finally {
    def.setLoading(false);
  }
}
